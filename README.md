# mpchart

## 简介
mpchart是一个包含各种类型图表的图表库，主要用于业务数据汇总，例如销售数据走势图，股价走势图等场景中使用，方便开发者快速实现图表UI，mpchart主要包括曲线图、柱形图、饼状图、蜡烛图、气泡图、雷达图、瀑布图等自定义图表库。

## 效果展示：
![gif](preview/preview.gif)
## 安装教程

```
 ohpm install @ohos/mpchart
```

OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明

### 开启最小单元刷新（局部刷新）
在项目的entry目录下的modele.json5的module属性里加上如下代码

```
  "metadata": [
      {
        "name": "ArkTSPartialUpdate",
        "value": "true"
      }
    ]
```

### 曲线图

1. 声明数据对象,以demo中Basic为例:

```
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  lineData: LineData | null = null;
  @State
  lineChartModel: LineChartModel = new LineChartModel();
```

2. 初始化数据

```
 public aboutToAppear() {
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title

    this.lineData = this.initCurveData(45, 180);

    this.topAxis.setLabelCount(5, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(44);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(44);
    this.bottomAxis.setDrawAxisLine(false);
    this.bottomAxis.setDrawLabels(false)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(-50);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.enableGridDashedLine(5,5,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(-50); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(200);
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false);

    let upperLimtLine:LimitLine= new LimitLine(150, "Upper Limit");
    upperLimtLine.setLineWidth(4);
    upperLimtLine.enableDashedLine(5, 5, 0);
    upperLimtLine.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
    upperLimtLine.setTextSize(10);

    let lowerLimtLine:LimitLine= new LimitLine(-30, "Lower Limit");
    lowerLimtLine.setLineWidth(4);
    lowerLimtLine.enableDashedLine(5, 5, 0);
    lowerLimtLine.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
    lowerLimtLine.setTextSize(10);

    this.leftAxis.setDrawLimitLinesBehindData(true);
    this.leftAxis.addLimitLine(upperLimtLine);
    this.leftAxis.addLimitLine(lowerLimtLine);
    
    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
     if (this.leftAxis) {
      this.lineChartModel.setLeftAxis(this.leftAxis);
    }
    if (this.rightAxis) {
      this.lineChartModel.setRightAxis(this.rightAxis);
    }
    if (this.lineData) {
      this.lineChartModel.setLineData(this.lineData);
    }
    this.lineChartModel.init();
  }
```

3. 添加数据到自定义曲线图表组件

```
 build() {
    Stack({ alignContent: Alignment.TopStart }) {
        LineChart({lineChartModel: this.lineChartModel})
    }
  }
```

### 柱形图

1. 竖向柱形图初始化数据

```
   aboutToAppear(){
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(100);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(100);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(12);

    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

  }
  private setData(count: number, range: number) {

        let start: number = 1;

        let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();

        for (let i = start; i < start + count; i++) {
            let val: number = (Math.random() * range);
            if (Math.random() * 100 < 25) {
                let paint:ImagePaint = new ImagePaint();
                paint.setIcon("app.media.star");
                values.add(new BarEntry(i, val, paint));
            } else {
                values.add(new BarEntry(i, val));
            }
        }

        let set1: BarDataSet;

        if (this.model.getBarData() != null &&
            this.model.getBarData().getDataSetCount() > 0) {
            set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
            set1.setValues(values);
            this.model.getBarData().notifyDataChanged();
            this.model.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "The year 2017");

            set1.setDrawIcons(false);

            let startColor1: number = 0xffffbb33;
            let startColor2 = 0xff33b5e5;
            let startColor3 = 0xffffbb33;
            let startColor4 = 0xff99cc00;
            let startColor5 = 0xffff4444;
            let endColor1 = 0xff0099cc;
            let endColor2 = 0xffaa66cc;
            let endColor3 = 0xff669900;
            let endColor4 = 0xffcc0000;
            let endColor5 = 0xffff8800;

            let gradientFills:JArrayList<Fill> = new JArrayList<Fill>();
            gradientFills.add(new Fill(undefined,startColor1, endColor1));
            gradientFills.add(new Fill(undefined,startColor2, endColor2));
            gradientFills.add(new Fill(undefined,startColor3, endColor3));
            gradientFills.add(new Fill(undefined,startColor4, endColor4));
            gradientFills.add(new Fill(undefined,startColor5, endColor5));

            set1.setFills(gradientFills);

            let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
            dataSets.add(set1);

            let data: BarData = new BarData(dataSets);
            data.setValueTextSize(10);
            data.setBarWidth(0.9);

            this.model.setData(data);
        }
    }
```

2. 添加数据到自定义竖向柱形图表组件

```
    build() {
     Stack({ alignContent: Alignment.TopStart }) {
     	BarChart({model:this.model})
     }
    }
```

3. 横向柱形图初始化数据

```
   aboutToAppear(){
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(12);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(100);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(100);

    this.setData(this.leftAxis.getAxisMaximum(),this.bottomAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setTopXAxis(this.topAxis);
    this.model.setBottomXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

  }
  private setData(count: number, range: number) {

    let start: number = 1;

    let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();

    for (let i = start; i < start + count; i++) {
      let val: number = (Math.random() * range);
      if (Math.random() * 100 < 25) {
        let paint:ImagePaint = new ImagePaint();
        paint.setIcon("app.media.star");
        values.add(new BarEntry(val, i, paint));
      } else {
        values.add(new BarEntry(val, i));
      }
    }

    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "The year 2017");

      set1.setDrawIcons(false);

      let startColor1: number = 0xffffbb33;
      let startColor2 = 0xff33b5e5;
      let startColor3 = 0xffffbb33;
      let startColor4 = 0xff99cc00;
      let startColor5 = 0xffff4444;
      let endColor1 = 0xff0099cc;
      let endColor2 = 0xffaa66cc;
      let endColor3 = 0xff669900;
      let endColor4 = 0xffcc0000;
      let endColor5 = 0xffff8800;

      let gradientFills:JArrayList<Fill> = new JArrayList<Fill>();
      gradientFills.add(new Fill(undefined,startColor1, endColor1));
      gradientFills.add(new Fill(undefined,startColor2, endColor2));
      gradientFills.add(new Fill(undefined,startColor3, endColor3));
      gradientFills.add(new Fill(undefined,startColor4, endColor4));
      gradientFills.add(new Fill(undefined,startColor5, endColor5));

      set1.setFills(gradientFills);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);
      data.setValueTextSize(10);
      data.setBarWidth(0.9);

      this.model.setData(data);
    }
  }
```

4. 添加数据到自定义横向柱形图表组件

```
    build() {
     Stack({ alignContent: Alignment.TopStart }) {
       BarChart({model:this.model})
     }
    }
```

### 瀑布图

1. 瀑布图初始化数据

```
   public aboutToAppear(){
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(40);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(40); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(200);

    this.bottomAxis.setLabelCount(7, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(7);

    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.setWidth(this.mWidth);
    this.model.setHeight(this.mHeight);
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
    this.model.setCustomCylinderWidth(20);
    this.model.setRadius(10);
    this.model.setCylinderCenter(true)
  }

  private setData(count: number, range: number) {

    let start: number = 1;

    let values:JArrayList<WaterfallEntry> = new JArrayList<WaterfallEntry>();
    let minimum = this.leftAxis.getAxisMinimum()
    values.add(new WaterfallEntry(1, [50 - minimum,150 - minimum],[50 - minimum]));
    values.add(new WaterfallEntry(2, [80 - minimum,120 - minimum],[120 - minimum]));
    values.add(new WaterfallEntry(3, [60 - minimum,160 - minimum],[60 - minimum,90 - minimum]));
    values.add(new WaterfallEntry(4, [90 - minimum,100 - minimum],[95 - minimum]));
    values.add(new WaterfallEntry(5, [40 - minimum,180 - minimum],[41 - minimum,45 - minimum]));
    values.add(new WaterfallEntry(6, [100 - minimum,200 - minimum],[100 - minimum,130 - minimum]));
    values.add(new WaterfallEntry(7, [55 - minimum,190 - minimum],[190 - minimum]));

    let set1: WaterfallDataSet;

    if (this.model.getWaterfallData() != null &&
    this.model.getWaterfallData().getDataSetCount() > 0) {
      set1 = this.model.getWaterfallData().getDataSetByIndex(0) as WaterfallDataSet;
      set1.setValues(values);
      this.model.getWaterfallData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new WaterfallDataSet(values, "Statistics Vienna 2014");

      set1.setDrawIcons(false);
      set1.setColorsByVariable(this.getColors());
      set1.setDotsColor(0xe74c3c)
      set1.setStackLabels(["Births", "Divorces", "Marriages"]);
      set1.setValueTextSize(8)

      let dataSets: JArrayList<IWaterfallDataSet> = new JArrayList<IWaterfallDataSet>();
      dataSets.add(set1);

      let data: WaterfallData = new WaterfallData(dataSets);
      data.setBarWidth(0);
      this.model.setData(data);
    }
  }

  private getColors(): number[]{

    let colors: number[] = [];

    colors.push(0x2ecc71)
    colors.push(0xf1c40f)

    return colors;
  }
```

2. 添加数据到瀑布图表组件

```
  build() {
    Column(){
      Stack({alignContent:Alignment.TopStart}){
        WaterfallChart({model:this.model})
      }
    }.alignItems(HorizontalAlign.Start)
  }
```

### 饼状图
1. 饼状图初始化数据

```
   aboutToAppear() {

    this.pieData = this.initPieData(4, 10);
    this.pieModel.setDrawAngles([80,40,140,100])
    .setPieData(this.pieData)
    .setRadius(500)
    .setHoleRadius(0.5)
   }
   private initPieData(count:number, range:number):PieData{
    let entries = new JArrayList<PieEntry>();
    for (let i = 0; i < count ; i++) {
      entries.add(new PieEntry(((Math.random() * range) + range / 5),this.parties[i % this.parties.length]))
    }

    let dataSet:PieDataSet = new PieDataSet(entries, "Election Results");
    dataSet.setDrawIcons(false);
    dataSet.setSliceSpace(3);
    dataSet.setIconsOffset(new MPPointF(0, 40));
    dataSet.setSelectionShift(5);

    // add a lot of colors
    let colors:JArrayList<number> = new JArrayList();
    for (let index = 0; index < ColorTemplate.VORDIPLOM_COLORS.length; index++) {
      colors.add( ColorTemplate.VORDIPLOM_COLORS[index]);
    }

    for (let index = 0; index < ColorTemplate.JOYFUL_COLORS.length; index++) {
      colors.add( ColorTemplate.JOYFUL_COLORS[index]);
    }

    for (let index = 0; index < ColorTemplate.COLORFUL_COLORS.length; index++) {
      colors.add( ColorTemplate.COLORFUL_COLORS[index]);
    }
    for (let index = 0; index < ColorTemplate.LIBERTY_COLORS.length; index++) {
      colors.add( ColorTemplate.LIBERTY_COLORS[index]);
    }
    for (let index = 0; index < ColorTemplate.PASTEL_COLORS.length; index++) {
      colors.add( ColorTemplate.PASTEL_COLORS[index]);
    }
    colors.add(ColorTemplate.getHoloBlue());
    dataSet.setColorsByList(colors);
    
    return new PieData(dataSet)
  }
```
2. 添加数据到自定义饼状图表组件
```
    build() {
     Stack({ alignContent: Alignment.TopStart }) {
      PieChart({
     	model:this.pieModel
      })
     }
    }
```

### 气泡图
1. 气泡图初始化数据

```
   topAxis: XAxis = new XAxis(); //顶部X轴
   bottomAxis: XAxis = new XAxis(); //底部X轴
   mWidth: number = 300; //表的宽度
   mHeight: number = 300; //表的高度
   minOffset: number =15; //X轴线偏移量
   leftAxis: YAxis = new YAxis();
   rightAxis: YAxis = new YAxis();
   data:BubbleData= new BubbleData();
   isDrawValuesEnable:boolean=false;
   @State
   bubbleChartMode:BubbleChartMode=new BubbleChartMode();
   
   aboutToAppear() {
    this.data=this.initBubbleData( 5,50)

    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(this.data.getXMax());
    this.topAxis.enableGridDashedLine(10,10,0)
    this.topAxis.setDrawAxisLine(true);
    this.topAxis.setDrawLabels(false)

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(this.data.getXMax());
    this.bottomAxis.setDrawAxisLine(true);
    this.bottomAxis.setDrawLabels(true)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(5, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(this.data.getYMax());
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.data.getYMax());
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false)

    let textPaint:TextPaint = new TextPaint();
    textPaint.setTextSize(this.leftAxis.getTextSize());
    let leftTextWidth=Utils.calcTextWidth(textPaint,this.getFormattedValue(this.leftAxis.getAxisMaximum()));
    let left = this.leftAxis.getSpaceTop() + leftTextWidth;
    let top = this.minOffset;
    let right = this.mWidth - this.minOffset - leftTextWidth;
    let bottom = this.mHeight - this.minOffset;
    this.data.mDisplayRect = new MyRect(left, top, right, bottom);

    this.bubbleChartMode.setYExtraOffset(this.model.height)
    this.bubbleChartMode.topAxis=this.topAxis
    this.bubbleChartMode.bottomAxis=this.bottomAxis
    this.bubbleChartMode.mWidth=this.mWidth
    this.bubbleChartMode.mHeight=this.mHeight
    this.bubbleChartMode.minOffset=this.minOffset
    this.bubbleChartMode.leftAxis=this.leftAxis
    this.bubbleChartMode.rightAxis=this.rightAxis
    this.bubbleChartMode.data=this.data
    this.bubbleChartMode.init()

    this.setXAxisMode();

    this.bubbleChartMode.leftAxisModel.setWidth(this.bubbleChartMode.mWidth)
    this.bubbleChartMode.leftAxisModel.setHeight(this.bubbleChartMode.mHeight)
    this.bubbleChartMode.leftAxisModel.setMinOffset(this.bubbleChartMode.minOffset)
    this.bubbleChartMode.leftAxisModel.setYAxis(this.bubbleChartMode.leftAxis)

    this.bubbleChartMode.rightAxisModel.setWidth(this.bubbleChartMode.mWidth)
    this.bubbleChartMode.rightAxisModel.setHeight(this.bubbleChartMode.mHeight)
    this.bubbleChartMode.rightAxisModel.setMinOffset(this.bubbleChartMode.minOffset)
    this.bubbleChartMode.rightAxisModel.setYAxis(this.bubbleChartMode.rightAxis)
    
  }
  public setXAxisMode(){
    this.bubbleChartMode.xAixsMode.topAxis=this.bubbleChartMode.topAxis
    this.bubbleChartMode.xAixsMode.bottomAxis=this.bubbleChartMode.bottomAxis,
    this.bubbleChartMode.xAixsMode.mWidth= this.bubbleChartMode.mWidth,
    this.bubbleChartMode.xAixsMode.mHeight=this.bubbleChartMode.mHeight,
    this.bubbleChartMode.xAixsMode.minOffset=this.bubbleChartMode.minOffset,
    this.bubbleChartMode.xAixsMode.yLeftLongestLabel=this.getFormattedValue(this.bubbleChartMode.leftAxis.getAxisMaximum()),
    this.bubbleChartMode.xAixsMode.yRightLongestLabel=this.getFormattedValue(this.bubbleChartMode.rightAxis.getAxisMaximum())
  }

  /**
     * 初始化数据
     * @param count  曲线图点的个数
     * @param range  y轴范围
     */
  private initBubbleData(count: number, range: number): BubbleData {

    let values1 = new JArrayList<BubbleEntry>();
    let values2 = new JArrayList<BubbleEntry>();
    let values3 = new JArrayList<BubbleEntry>();
    let imgePaint:ImagePaint=new ImagePaint();
    imgePaint.setIcon($r('app.media.star'))
    imgePaint.setWidth(px2vp(32))
    imgePaint.setHeight(px2vp(32));
    for (let i = 0; i < count; i++) {
      values1.add(new BubbleEntry(i, Math.random() * range, Math.random() * range, imgePaint));
      values2.add(new BubbleEntry(i, Math.random() * range, Math.random() * range, imgePaint));
      values3.add(new BubbleEntry(i, Math.random() * range, Math.random() * range));
    }

    let set1:BubbleDataSet = new BubbleDataSet(values1, "DS 1");
    set1.setDrawIcons(false);
    set1.setColorByColor(0x88c12552);
    set1.setIconsOffset(new MPPointF(0, px2vp(0)));
    set1.setDrawValues(this.isDrawValuesEnable);


    let  set2:BubbleDataSet = new BubbleDataSet(values2, "DS 2");
    set2.setDrawIcons(false);
    set2.setIconsOffset(new MPPointF(0, px2vp(0)));
    set2.setColorByColor(0x88ff6600);
    set2.setDrawValues(this.isDrawValuesEnable);

    let set3:BubbleDataSet = new BubbleDataSet(values3, "DS 3");
    set3.setDrawIcons(false);
    set3.setIconsOffset(new MPPointF(0, 0));
    set3.setColorByColor(0x88f5c700);
    set3.setDrawValues(this.isDrawValuesEnable);


    let dataSets = new JArrayList<IBubbleDataSet>();
    dataSets.add(set1);
    dataSets.add(set2);
    dataSets.add(set3);
    let dataResult:BubbleData = new BubbleData(dataSets);
    dataResult.setDrawValues(this.isDrawValuesEnable);
    dataResult.setValueTextSize(8);
    dataResult.setValueTextColor(Color.White);
    dataResult.setHighlightCircleWidth(1.5);
    dataResult.setHighlightEnabled(true);
    return dataResult;
  }
```
2. 添加数据到自定义气泡图表组件
```
 build() {
    Stack({ alignContent: Alignment.TopStart }) {
       BubbleChart({
          bubbleChartMode:$bubbleChartMode
        })
    }
  }
```

### candleStick蜡烛图
1. candleStick蜡烛图初始化数据

```
  @State chartModel: CandleStickChart.Model = new CandleStickChart.Model()
  
  aboutToAppear() {
    this.candleData = this.initCandleData(40, 100);
    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title
    this.chartModel.setCandleData(this.candleData)
  }
  
  private initCandleData(count: number, ranged: number): MyCandleData {
    let values = new JArrayList<MyCandleEntry>();

    values.add(new MyCandleEntry(40, 580, 380, 520, 430));
    values.add(new MyCandleEntry(60, 510, 300, 350, 440));
    values.add(new MyCandleEntry(80, 520, 320, 480, 380));
    values.add(new MyCandleEntry(100, 580, 300, 400, 500));
    values.add(new MyCandleEntry(120, 580, 400, 560, 460));
    values.add(new MyCandleEntry(140, 540, 320, 370, 470));
    values.add(new MyCandleEntry(160, 460, 230, 400, 300));
    values.add(new MyCandleEntry(180, 520, 330, 390, 480));
    values.add(new MyCandleEntry(200, 590, 350, 530, 450));
    values.add(new MyCandleEntry(220, 540, 380, 420, 490));
    values.add(new MyCandleEntry(240, 530, 260, 460, 350));
    values.add(new MyCandleEntry(260, 450, 240, 300, 390));
    values.add(new MyCandleEntry(280, 300, 100, 240, 160));
    values.add(new MyCandleEntry(300, 240, 30, 90, 180));
    values.add(new MyCandleEntry(320, 570, 360, 510, 420));
    values.add(new MyCandleEntry(340, 440, 220, 280, 380));

    let dataSets = new JArrayList<MyCandleDataSet>();

    let set1 = new MyCandleDataSet(values, "Data Set");
    dataSets.add(set1);

    return new MyCandleData(dataSets);
  }
```

2. 添加数据到自定义candleStick蜡烛图表组件
```
 build() {
    Stack({ alignContent: Alignment.TopStart }) {
        CandleStickChart({ model: $chartModel })
    }
  }
```

### ScatterChart散点图
1. ScatterChart散点图初始化数据

```
  @State
  scaterChartMode: ScaterChartMode = new ScaterChartMode();
  
  aboutToAppear() {
    this.scatterData = this.initCurveData(250, 250);

    //设置顶部轴
    this.topAxis.setLabelCount(6, false); //顶部标签个数
    this.topAxis.setPosition(XAxisPosition.TOP);//设置轴的位置
    this.topAxis.setAxisMinimum(0);//设置轴的最小值
    this.topAxis.setAxisMaximum(250);//设置轴的最大值
    this.topAxis.setDrawGridLines(false);//设置是否绘制网格线

    //设置底部轴
    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(250);

    //设置左侧轴
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(250);
    this.leftAxis.enableGridDashedLine(10, 10, 0)

    //设置右侧轴
    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setDrawLabels(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(250);
    this.rightAxis.setEnabled(false);

    this.scaterChartMode.setYExtraOffset(this.model.height) 
    this.scaterChartMode.topAxis = this.topAxis
    this.scaterChartMode.bottomAxis = this.bottomAxis
    this.scaterChartMode.mWidth = this.mWidth
    this.scaterChartMode.mHeight = this.mHeight
    this.scaterChartMode.minOffset = this.minOffset
    this.scaterChartMode.leftAxis = this.leftAxis
    this.scaterChartMode.rightAxis = this.rightAxis
    this.scaterChartMode.data = this.scatterData
    this.scaterChartMode.XLimtLine = this.XLimtLine
    this.setDisplayRect();
    this.scaterChartMode.init()
  }
  
  /**
   * 初始化数据
   * @param xRange  x轴范围
   * @param yRange  y轴范围
   */
  private initCurveData(xRange: number, yRange: number): ScatterData {
    //生成随机数据1
    let values = this.generateRandomData(xRange, yRange);
    //生成随机数据2
    let values2 = this.generateRandomData(xRange, yRange);
    //生成随机数据3
    let values3 = this.generateRandomData(xRange, yRange);

    //创建一个数据并给它一个类型标签，这里的标签是“DS 1”
    let set1: ScatterDataSet = new ScatterDataSet(values, "DS 1");
    // 设置散点形状，形状类型有以下几种
    // SQUARE 正方形
    // TRIANGLE 三角形
    // CIRCLE 圆形
    // CROSS 十字架形
    // X X形
    // CHEVRON_UP 向上箭头形
    // CHEVRON_DOWN 向下箭头形
    set1.setScatterShape(ScatterShape.SQUARE);
    //设置图形偏移量
    set1.setIconsOffset(new MPPointF(0, px2vp(0)));
    //    set1.setScatterShapeHoleRadius(3);
    //    set1.setScatterShape(ScatterShape.TRIANGLE);
    //    set1.setScatterShape(ScatterShape.CROSS);
    //    set1.setScatterShape(ScatterShape.CHEVRON_DOWN);
    //设置颜色
    set1.setColorByColor(ColorTemplate.COLORFUL_COLORS[0]);
    //设置是否绘制数值文本
    set1.setDrawValues(this.isDrawValuesEnable);
    //设置高亮线的宽度
    set1.setHighlightLineWidth(px2vp(1))

    let set2: ScatterDataSet = new ScatterDataSet(values2, "DS 2");
    set2.setScatterShape(ScatterShape.CIRCLE);
    set2.setScatterShapeHoleColor(ColorTemplate.COLORFUL_COLORS[3]);
    //    set2.setScatterShapeHoleRadius(3);
    set2.setIconsOffset(new MPPointF(0, px2vp(0)));
    set2.setColorByColor(ColorTemplate.COLORFUL_COLORS[1]);
    set2.setDrawValues(this.isDrawValuesEnable);
    set2.setHighlightLineWidth(px2vp(1))

    let set3: ScatterDataSet = new ScatterDataSet(values3, "DS 3");
    //    set3.setScatterShape(ScatterShape.X);
    //    set3.setScatterShape(ScatterShape.CHEVRON_UP);
    //    set3.setScatterShape(ScatterShape.CHEVRON_DOWN);
    //    set3.setScatterShape(ScatterShape.TRIANGLE);
    //    set3.setScatterShapeHoleRadius(3);
    set3.setShapeRenderer(new CustomScatterShapeRenderer());
    //    set3.setScatterShapeHoleRadius(3);
    set3.setColorByColor(ColorTemplate.COLORFUL_COLORS[2]);
    set3.setIconsOffset(new MPPointF(0, px2vp(0)));
    set3.setDrawValues(this.isDrawValuesEnable);
    set3.setHighlightLineWidth(px2vp(1))

    set1.setScatterShapeSize(8);
    set2.setScatterShapeSize(8);
    set3.setScatterShapeSize(8);

    let dataSets: JArrayList<IScatterDataSet> = new JArrayList<IScatterDataSet>();
    dataSets.add(set1); // add the data sets
    dataSets.add(set2);
    dataSets.add(set3);

    let dataResult: ScatterData = new ScatterData(dataSets);
    dataResult.setDrawValues(this.isDrawValuesEnable);
    dataResult.setValueTextSize(8);
    dataResult.setHighlightEnabled(true);

    return dataResult;
  }

  private generateRandomData(xRange: number, yRange: number): JArrayList<EntryOhos> {
    let values = new JArrayList<EntryOhos>();
    for (let i = 0; i < 5; i++) {
      let x = Math.random() * xRange; // Random x value within specified count.
      let y = Math.random() * yRange; // Random y value within specified range.
      values.add(new EntryOhos(x, y));
    }
    return values;
  }
```

2. 添加数据到自定义scaterChartMode散点图表组件
```
 build() {
    ScatterChart({
       scaterChartMode: this.scaterChartMode
    })
  }
```
## 接口说明
`let values = new JArrayList<Number>();`
1. 添加数据
   `values.append(element:T)`
2. 移除数据
   `values.remove(element:T)`
3. 获取数据
   `values.get(pos:number)`
4. 清除数据
   `values.clear()`

`let barEntry=new BarEntry(35, [ -17, 17 ]);`
1. 复制对象
   `barEntry.copy()`
2. 设置Y轴数组
   `barEntry.setVals(vals: number[])`
3. 是否层叠
   `barEntry.isStacked()`
4. 计算Y轴最小值
   `barEntry.calcYValsMin()`
5. 计算Y轴范围
   `barEntry.calcRanges()`

`let paint : Paint = new Paint();`
1. 设置颜色
   `paint.setColor(value: Color | number | string | Resource)`
2. 设置文字大小
   `paint.setTextSize(value: number)`
3. 社会填充
   `paint.setFill(value: Color | number | string | Resource)`
4. 设置边框颜色
   `paint.setStroke(value: Color | number | string | Resource)`
5. 设置透明度
   `paint.setAlpha(value: number)`

`let set1:BubbleDataSet = new BubbleDataSet(values1, "DS 1");`
1. 设置是否需要绘制图标
   `set1.setDrawIcons(false)`
2. 设置绘制颜色
   `set1.setColorByColor(0x88c12552);`
3. 设置图标偏移距离
   `set1.setIconsOffset(new MPPointF(0, px2vp(0)))`
4. 设置是否绘制值
   `set1.setDrawValues(true);`

`let set1: ScatterDataSet  = new ScatterDataSet(values, "DS 1");`
1. 设置绘制形状
   `set1.setScatterShape(ScatterShape.SQUARE)`
2. 设置绘制半径
   ` set1.setScatterShapeHoleRadius(3);`
3. 设置绘制图形大小
   `set1.setScatterShapeSize(8);`

`let topAxis: XAxis = new XAxis();`
1. 设置轴文字label数量
   `topAxis.setLabelCount(6, false);`
2. 设置轴文本位置
   `topAxis.setPosition(XAxisPosition.TOP)`
3. 设置最小值
   `topAxis.setAxisMinimum(10)`
4. 设置最大自值
   `topAxis.setAxisMaximum(50)`
5. 设置网格线虚线间隔
   `topAxis.enableGridDashedLine(10,10,0)`

`let model:HorizontalBarChartModel = new HorizontalBarChartModel();`
1. 初始化数据
   `model.init();`
2. 计算值数据
   `model.prepareMatrixValuePx()`
3. 设置最大显示label数量
   `model.setMaxVisibleValueCount(count: number)`
4. 设置描述对象
   `model.setDescription(desc: Description)`
5. 设置左侧Y轴
   `model.setLeftYAxis(axis:YAxis)`
6. 设置底部X轴
   `model.setBottomXAxis(axis:XAxis)`

`let rect: MyRect = new MyRect();`
1. 复制对象
   `rect.copyOrNull(r: MyRect);`
2. 对比对象
   `rect.equals(o: Object):`
3. 计算宽度
   `rect.width()`
4. 计算高度
   `rect.height()`
5. 计算中心点X轴值
   `rect.centerX()`
6. 计算中心点Y轴值
   `rect.centerY()`

`let data: BarData = new BarData(dataSets);`
1. 设置圆柱的宽度
  `data.setBarWidth(0.5);` //设置为 0 使用的是默认宽度，不设置时宽度为0.85
   
   

## 约束与限制
在下述版本验证通过：

- DevEco Studio: 4.0 (4.0.3.513), SDK: API10 (4.0.10.10)
- DevEco Studio: 4.0 (4.0.3.501), --SDK (4.0.10.8)

## 目录结构

````
|---- ohos-MPChart
|     |---- entry  # 示例代码文件夹
|     |---- mpchart  # mpchart库文件夹
|           |---- index.ets  # 对外接口
│           ├----components # 框架代码目录
│                 ├----animation # 动画目录
│                 │      
│                 ├----buffer # 缓存相关目录
│                 │      
│                 ├----charts # 各类型图表目录
│                 │      
│                 ├----components   # 自定义组件目录
│                 │          
│                 ├----data   # 数据实体目录
│                 │          
│                 ├----exception # 异常处理目录
│                 │      
│                 ├----formatter # 各种数据格式化目录
│                 │      
│                 ├----highlight # 各种图表中高亮显示操作目录
│                 │      
│                 ├----interfaces   # 对外接口目录
│                 │          
│                 ├----jobs   # 动画工作线程目录
│                 │      
│                 ├----listener  # 手势监听目录
│                 │      
│                 ├----matrix # 3D 效果计算目录
│                 │      
│                 ├----model  # 过渡颜色
│                 │      
│                 ├----renderer  # 各种图表绘制属性设置目录
│                 │          
│                 └----utils  # 工具类目录
│                  
└─resources # 资源文件
|     |---- README.md  # 安装使用方法  

````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos-MPChart/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/ohos-MPChart/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-MPChart/blob/master/LICENSE) ，请自由地享受和参与开源。