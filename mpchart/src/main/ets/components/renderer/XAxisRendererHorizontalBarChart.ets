/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Paint,{Style,PathPaint,LinePaint}from '../data/Paint';
import {XAxis,XAxisPosition} from '../components/XAxis';
import LimitLine,{LimitLabelPosition} from '../components/LimitLine';
import BarChart from '../charts/BarChart';
import MyRect from '../data/Rect';
import FSize from '../utils/FSize';
import MPPointF from '../utils/MPPointF';
import MPPointD from '../utils/MPPointD';
import Transformer from '../utils/Transformer';
import Utils from '../utils/Utils';
import ViewPortHandler from '../utils/ViewPortHandler';
import {JArrayList} from '../utils/JArrayList';
import XAxisRenderer from './XAxisRenderer';

export default class XAxisRendererHorizontalBarChart extends XAxisRenderer {

    protected mChart:BarChart;

    constructor( viewPortHandler:ViewPortHandler,  xAxis:XAxis,trans:Transformer,  chart:BarChart) {
        super(viewPortHandler, xAxis, trans);
        this.mChart = chart;
    }

    public computeAxis( min:number,  max:number,  inverted:boolean):void {

        // calculate the starting and entry point of the y-labels (depending on
        // zoom / contentrect bounds)
        if (this.mViewPortHandler.contentWidth() > 10 && !this.mViewPortHandler.isFullyZoomedOutY()) {

            let p1:MPPointD = this.mTrans.getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentBottom());
            let p2:MPPointD = this.mTrans.getValuesByTouchPoint(this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop());

            if (inverted) {
                min = p2.y;
                max =p1.y;
            } else {

                min =p1.y;
                max =p2.y;
            }

            MPPointD.recycleInstance(p1);
            MPPointD.recycleInstance(p2);
        }

        this.computeAxisValues(min, max);
    }
    
    protected  computeSize():void {
        
        this.mAxisLabelPaint.setTypeface(this.mXAxis.getTypeface());
        this.mAxisLabelPaint.setTextSize(this.mXAxis.getTextSize());

        let longest:string = this.mXAxis.getLongestLabel();

        let labelSize:FSize = Utils.calcTextSize(this.mAxisLabelPaint, longest);

        let labelWidth:number = labelSize.width + this.mXAxis.getXOffset() * 3.5;
        let labelHeight:number = labelSize.height;

        let labelRotatedSize:FSize = Utils.getSizeOfRotatedRectangleByDegrees(
                labelSize.width,
                labelHeight,
                this.mXAxis.getLabelRotationAngle());

        this.mXAxis.mLabelWidth = Math.round(labelWidth);
        this.mXAxis.mLabelHeight = Math.round(labelHeight);
        this.mXAxis.mLabelRotatedWidth = labelRotatedSize.width + this.mXAxis.getXOffset() * 3.5;
        this.mXAxis.mLabelRotatedHeight = Math.round(labelRotatedSize.height);

        FSize.recycleInstance(labelRotatedSize);
    }

    public renderAxisLabels():Paint[] {

        if (!this.mXAxis.isEnabled() || !this.mXAxis.isDrawLabelsEnabled())
            return;

        let xoffset:number = this.mXAxis.getXOffset();

        this.mAxisLabelPaint.setTypeface(this.mXAxis.getTypeface());
        this.mAxisLabelPaint.setTextSize(this.mXAxis.getTextSize());
        this.mAxisLabelPaint.setColor(this.mXAxis.getTextColor());

        let pointF:MPPointF = MPPointF.getInstance(0,0);

        if (this.mXAxis.getPosition() == XAxisPosition.TOP) {
            pointF.x = 0.0;
            pointF.y = 0.5;
            return this.drawLabels( this.mViewPortHandler.contentRight() + xoffset, pointF);

        } else if (this.mXAxis.getPosition() == XAxisPosition.TOP_INSIDE) {
            pointF.x = 1.0;
            pointF.y = 0.5;
            return this.drawLabels(this.mViewPortHandler.contentRight() - xoffset, pointF);

        } else if (this.mXAxis.getPosition() == XAxisPosition.BOTTOM) {
            pointF.x = 1.0;
            pointF.y = 0.5;
            return this.drawLabels(this.mViewPortHandler.contentLeft() - xoffset, pointF);

        } else if (this.mXAxis.getPosition() == XAxisPosition.BOTTOM_INSIDE) {
            pointF.x = 1.0;
            pointF.y = 0.5;
            return this.drawLabels(this.mViewPortHandler.contentLeft() + xoffset, pointF);

        } else { // BOTH SIDED
            let paintArr:Paint[]=new Array();
            pointF.x = 0.0;
            pointF.y = 0.5;
            paintArr.concat(this.drawLabels(this.mViewPortHandler.contentRight() + xoffset, pointF));
            pointF.x = 1.0;
            pointF.y = 0.5;
            paintArr.concat(this.drawLabels(this.mViewPortHandler.contentLeft() - xoffset, pointF));
            return paintArr;
        }

        MPPointF.recycleInstance(pointF);
    }

    protected drawLabels( pos:number, anchor:MPPointF):Paint[] {
        let labelPaint:Paint[]=new Array();
        let labelRotationAngleDegrees:number = this.mXAxis.getLabelRotationAngle();
        let centeringEnabled:boolean = this.mXAxis.isCenterAxisLabelsEnabled();

        let positions:number[] = new Array(this.mXAxis.mEntryCount * 2);

        for (let i = 0; i < positions.length; i += 2) {

            // only fill x values
            if (centeringEnabled) {
                positions[i + 1] = this.mXAxis.mCenteredEntries[i / 2];
            } else {
                positions[i + 1] = this.mXAxis.mEntries[i / 2];
            }
        }

        this.mTrans.pointValuesToPixel(positions);

        for (let i = 0; i < positions.length; i += 2) {

            let y:number = positions[i + 1];

            if (this.mViewPortHandler.isInBoundsY(y)) {

                let label:string = this.mXAxis.getValueFormatter().getFormattedValue(this.mXAxis.mEntries[i / 2], this.mXAxis);
                labelPaint.push(this.drawLabel(label, pos, y, anchor, labelRotationAngleDegrees));
            }
        }
      return null
    }

    public getGridClippingRect():MyRect {
        this.mGridClippingRect.set(this.mViewPortHandler.getContentRect().left,this.mViewPortHandler.getContentRect().top
          ,this.mViewPortHandler.getContentRect().right,this.mViewPortHandler.getContentRect().bottom);
        this.mGridClippingRect.inset(0, -this.mAxis.getGridLineWidth(), 0, -this.mAxis.getGridLineWidth());
        return this.mGridClippingRect;
    }

    protected drawGridLine(x:number, y:number,  gridLinePath:string):Paint {
        //let xResult:number=this.calcXLeftOffset(x)
        gridLinePath="M"+Utils.convertDpToPixel(this.mViewPortHandler.contentRight())+" "+Utils.convertDpToPixel(y)+"L"+Utils.convertDpToPixel(this.mViewPortHandler.contentTop())+" "+Utils.convertDpToPixel(y)
        let pathPaint:PathPaint=new PathPaint(this.mGridPaint as PathPaint);
        pathPaint.setCommands(gridLinePath);
        return pathPaint
    }

    public renderAxisLine():Paint[] {

        if (!this.mXAxis.isDrawAxisLineEnabled() || !this.mXAxis.isEnabled())
            return;

        this.mAxisLinePaint.setColor(this.mXAxis.getAxisLineColor());
        this.mAxisLinePaint.setStrokeWidth(this.mXAxis.getAxisLineWidth());
        let linePaint:LinePaint= new LinePaint(this.mAxisLinePaint as LinePaint);
        if (this.mXAxis.getPosition() == XAxisPosition.TOP
                || this.mXAxis.getPosition() == XAxisPosition.TOP_INSIDE
                || this.mXAxis.getPosition() == XAxisPosition.BOTH_SIDED) {
            linePaint.setStartPoint([this.mViewPortHandler.contentRight(), this.mViewPortHandler.contentTop()])
            linePaint.setEndPoint([this.mViewPortHandler.contentRight(),this.mViewPortHandler.contentBottom()])
        }

        if (this.mXAxis.getPosition() == XAxisPosition.BOTTOM
                || this.mXAxis.getPosition() == XAxisPosition.BOTTOM_INSIDE
                || this.mXAxis.getPosition() == XAxisPosition.BOTH_SIDED) {
            linePaint.setStartPoint([this.mViewPortHandler.contentLeft(), this.mViewPortHandler.contentTop()])
            linePaint.setEndPoint([this.mViewPortHandler.contentLeft(),this.mViewPortHandler.contentBottom()])
        }
      return [linePaint];
    }

    protected  mRenderLimitLinesPathBuffer:PathPaint = new PathPaint();
    /**
	 * Draws the LimitLines associated with this axis to the screen.
	 * This is the standard YAxis renderer using the XAxis limit lines.
	 *
	 * @param c
	 */
	public renderLimitLines():Paint[] {

	  let limitLines:JArrayList<LimitLine> = this.mXAxis.getLimitLines();

		if (limitLines == null || limitLines.size() <= 0)
			return;

		let pts:number[] = this.mRenderLimitLinesBuffer;
        pts[0] = 0;
        pts[1] = 0;

		let limitLinePath:PathPaint = this.mRenderLimitLinesPathBuffer;
        limitLinePath.reset();

		for (let i = 0; i < limitLines.size(); i++) {

			let l:LimitLine = limitLines.get(i);

            if(!l.isEnabled())
                continue;


      this.mLimitLineClippingRect.set(this.mViewPortHandler.getContentRect().left,this.mViewPortHandler.getContentRect().top
        ,this.mViewPortHandler.getContentRect().right,this.mViewPortHandler.getContentRect().bottom);
      this.mLimitLineClippingRect.inset(0, -l.getLineWidth(), 0, -l.getLineWidth());

			this.mLimitLinePaint.setStyle(Style.STROKE);
			this.mLimitLinePaint.setColor(l.getLineColor());
			this.mLimitLinePaint.setStrokeWidth(l.getLineWidth());
			this.mLimitLinePaint.setDashPathEffect(l.getDashPathEffect());

			pts[1] = l.getLimit();

			this.mTrans.pointValuesToPixel(pts);

			limitLinePath.moveTo(this.mViewPortHandler.contentLeft(), pts[1]);
			limitLinePath.lineTo(this.mViewPortHandler.contentRight(), pts[1]);

//			c.drawPath(limitLinePath, this.mLimitLinePaint);
//			limitLinePath.reset();
			// c.drawLines(pts, mLimitLinePaint);

			let label:string = l.getLabel();

			// if drawing the limit-value label is enabled
			if (label != null && label!=undefined&&label.length>0) {

				this.mLimitLinePaint.setStyle(l.getTextStyle());
				this.mLimitLinePaint.setDashPathEffect(null);
				this.mLimitLinePaint.setColor(l.getTextColor());
				this.mLimitLinePaint.setStrokeWidth(0.5);
				this.mLimitLinePaint.setTextSize(l.getTextSize());

        let labelLineHeight:number = Utils.calcTextHeight(this.mLimitLinePaint, label);
        let xOffset:number = Utils.convertDpToPixel(4) + l.getXOffset();
        let yOffset:number = l.getLineWidth() + labelLineHeight + l.getYOffset();

        let  position:LimitLabelPosition = l.getLabelPosition();

				if (position == LimitLabelPosition.RIGHT_TOP) {

					this.mLimitLinePaint.setTextAlign(TextAlign.End);
//					c.drawText(label,
//                            mViewPortHandler.contentRight() - xOffset,
//							pts[1] - yOffset + labelLineHeight, mLimitLinePaint);

				} else if (position == LimitLabelPosition.RIGHT_BOTTOM) {

                    this.mLimitLinePaint.setTextAlign(TextAlign.End);
//                    c.drawText(label,
//                            mViewPortHandler.contentRight() - xOffset,
//                            pts[1] + yOffset, mLimitLinePaint);

                } else if (position == LimitLabelPosition.LEFT_TOP) {

                    this.mLimitLinePaint.setTextAlign(TextAlign.Start);
                   /* c.drawText(label,
                            mViewPortHandler.contentLeft() + xOffset,
                            pts[1] - yOffset + labelLineHeight, mLimitLinePaint);*/

                } else {

					this.mLimitLinePaint.setTextAlign(TextAlign.Start);
				/*	c.drawText(label,
                            mViewPortHandler.offsetLeft() + xOffset,
							pts[1] + yOffset, mLimitLinePaint);*/
				}
			}

            //c.restoreToCount(clipRestoreCount);
		}
	}
}
