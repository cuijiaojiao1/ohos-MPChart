/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ScaterChartMode from '../data/ScaterChartMode';
import MPPointD from '../utils/MPPointD';
import Highlight from '../highlight/Highlight';
import MPPointF from '../utils/MPPointF';
import { JArrayList } from '../utils/JArrayList';
import EntryOhos from '../data/EntryOhos';
import Transformer from '../utils/Transformer';
import Paint, { ImagePaint, PathPaint, LinePaint, TextPaint }  from '../data/Paint';
import ScatterData from '../data/ScatterData';
import ViewPortHandler from '../utils/ViewPortHandler';
import ChartAnimator from '../animation/ChartAnimator';
import LineScatterCandleRadarRenderer from '../renderer/LineScatterCandleRadarRenderer'
import IScatterDataSet from '../interfaces/datasets/IScatterDataSet'
import IShapeRenderer from '../renderer/scatter/IShapeRenderer'
import Utils from '../utils/Utils';
import MyRect from '../data/Rect';
import IBarLineScatterCandleBubbleDataSet from '../interfaces/datasets/IBarLineScatterCandleBubbleDataSet'
import { DataSet, Rounding } from '../data/DataSet';

export default class ScatterChartRenderer extends LineScatterCandleRadarRenderer {
  scaterChartMode: ScaterChartMode;
  constructor(scaterChartMode: ScaterChartMode) {
    super(scaterChartMode.mAnimator, scaterChartMode.handler);
    this.scaterChartMode = scaterChartMode;
  }

  public initBuffers() {
  }

  public drawData(): Paint[] {

    let paints: Paint[] = [];

    for (let i = 0;i < this.scaterChartMode.data.getDataSetCount(); i++) {
      let dataSet: IScatterDataSet | null= this.scaterChartMode.data.getDataSetByIndex(i);

      if (dataSet && dataSet.isVisible()) {
        let drawPaints = this.drawDataSet(dataSet);
        if (drawPaints) {
          paints = paints.concat(drawPaints);
        }
      }
    }
    return paints;
  }

  private mPixelBuffer: Array<number> = new Array<number>(2);

  protected drawDataSet(dataSet: IScatterDataSet): Paint[] | null {

    if (dataSet.getEntryCount() < 1)
    return null;

    let viewPortHandler: ViewPortHandler | null = this.mViewPortHandler;

    //    let trans:Transformer = this.mChart.getTransformer(dataSet.getAxisDependency());
    if (this.mViewPortHandler) {
      let trans: Transformer = new Transformer(this.mViewPortHandler);

      let phaseY: number = this.mAnimator.getPhaseY();

      let renderer: IShapeRenderer = dataSet.getShapeRenderer();
      if (renderer == null) {
        console.info("MISSING\t" + "There's no IShapeRenderer specified for ScatterDataSet")
        return null;
      }

      let max: number = (Math.min(
        Math.ceil(dataSet.getEntryCount() * this.mAnimator.getPhaseX()), dataSet.getEntryCount()));
      let paints: Paint[] = [];

      for (let i = 0; i < max; i++) {
        if (i > this.scaterChartMode.maxVisiableIndex) {
          break;
        }
        let e: EntryOhos | null = dataSet.getEntryForIndex(i);

        if (e) {
          this.mPixelBuffer[0] = (this.calcXDataResult(e.getX(), 0) * this.scaterChartMode.scaleX + this.scaterChartMode.moveX) - this.scaterChartMode.currentXSpace
          this.mPixelBuffer[1] = (this.calcYDataResult(e.getY() * phaseY, 0) * this.scaterChartMode.scaleY + this.scaterChartMode.moveY) - this.scaterChartMode.currentYSpace
        }
        trans.pointValuesToPixel(this.mPixelBuffer);

        //      if (!viewPortHandler.isInBoundsRight(this.mPixelBuffer[0]))
        //      break;
        //
        //      if (!viewPortHandler.isInBoundsLeft(this.mPixelBuffer[0])
        //      || !viewPortHandler.isInBoundsY(this.mPixelBuffer[1]))
        //      continue;
        if (this.mRenderPaint) {
          this.mRenderPaint.setColor(dataSet.getColor(i));
          renderer.renderShape(paints, dataSet, this.mViewPortHandler,
            this.mPixelBuffer[0], this.mPixelBuffer[1],
            this.mRenderPaint)
        }

      }
      return paints;
    }
    return null;
  }

  /**
     * Returns the lowest x-index (value on the x-axis) that is still visible on
     * the chart.
     *
     * @return
     */
  public getLowestVisibleX(): number{
    if (!this.mViewPortHandler) {
      return 0;
    }
      let trans: Transformer = new Transformer(this.mViewPortHandler);
      let posForGetLowestVisibleX: MPPointD = MPPointD.getInstance(0, 0);
      let mpPointD: MPPointD = trans.getValuesByTouchPoint(this.mViewPortHandler.contentLeft(),
        this.mViewPortHandler.contentBottom(), posForGetLowestVisibleX);
      let mAxisMinimum = 0;
      let result: number = Math.max(mAxisMinimum, posForGetLowestVisibleX.x);
      return result;
  }

  /**
     * Returns the highest x-index (value on the x-axis) that is still visible
     * on the chart.
     *
     * @return
     */
  public getHighestVisibleX(): number {
    if (!this.mViewPortHandler) {
      return 0;
    }
    let trans: Transformer = new Transformer(this.mViewPortHandler);
    let posForGetHighestVisibleX: MPPointD = MPPointD.getInstance(0, 0);
    let mpPointD: MPPointD = trans.getValuesByTouchPoint(this.mViewPortHandler.contentRight(),
    this.mViewPortHandler.contentBottom(), posForGetHighestVisibleX);
    let mAxisMaximum = 250;
    let result: number = Math.min(mAxisMaximum, posForGetHighestVisibleX.x);
    return result;
  }

  private set(dataSet: IBarLineScatterCandleBubbleDataSet<EntryOhos>) {
    let phaseX: number = Math.max(0, Math.min(1, this.mAnimator.getPhaseX()));

    //    let low: number = chart.getLowestVisibleX();
    //    let high: number = chart.getHighestVisibleX();
    let low: number = this.getLowestVisibleX();
    let high: number = this.getHighestVisibleX();

    let entryFrom: EntryOhos | null = dataSet.getEntryForXValue(low, Number.NaN, Rounding.DOWN);
    let entryTo: EntryOhos | null= dataSet.getEntryForXValue(high, Number.NaN, Rounding.UP);
    if (entryFrom && entryTo) {
      this.mXBounds.min = entryFrom == null ? 0 : dataSet.getEntryIndexByEntry(entryFrom);
      this.mXBounds.max = entryTo == null ? 0 : dataSet.getEntryIndexByEntry(entryTo);
    }
  }

  public drawValues(): Paint[] {

    let paints: Paint[] = [];

    const dataSets: JArrayList<IScatterDataSet> | null= this.scaterChartMode.data.getDataSets();
    if (dataSets) {
      for (let i = 0; i < dataSets.size(); i++) {

        let dataSet: IScatterDataSet = dataSets.get(i);

        if (dataSet.getEntryCount() < 1)
          continue;

        // apply the text-styling defined by the DataSet
        this.applyValueTextStyle(dataSet);
        //        this.mXBounds.set(mChart, dataSet, this.mAnimator);
        //        this.set(dataSet);
        //        let trans:Transformer = new Transformer(this.mViewPortHandler);
        //        let positions: number[] = trans.generateTransformedValuesScatter(dataSet,
        //          this.mAnimator.getPhaseX(), this.mAnimator.getPhaseY(), this.mXBounds.min, this.mXBounds.max);

        let shapeSize: number = Utils.convertDpToPixel(dataSet.getScatterShapeSize());

        let iconsOffset: MPPointF = MPPointF.getInstance(undefined, undefined, dataSet.getIconsOffset());
        iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
        iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);
        for (let j = 0; j < dataSet.getEntryCount(); j++) {
          if (j > this.scaterChartMode.maxVisiableIndex) {
            break;
          }
          //           if (!this.mViewPortHandler.isInBoundsRight(positions[j]))
          //           break;
          //
          //           // make sure the lines don't do shitty things outside bounds
          //           if ((!this.mViewPortHandler.isInBoundsLeft(positions[j])
          //           || !this.mViewPortHandler.isInBoundsY(positions[j + 1])))
          //           continue;

          let entry: EntryOhos | null= dataSet.getEntryForIndex(j);
          if (!entry){
           continue
          }
          if (dataSet.isDrawValuesEnabled()) {
            let valueFormatter = dataSet.getValueFormatter();
            if (valueFormatter && this.mValuePaint &&  this.mViewPortHandler && this.scaterChartMode.mAnimator) {
              let value = valueFormatter.getFormattedValue(entry.getY(), entry, i, this.mViewPortHandler)
              let x: number = this.calcXDataResult(entry.getX(), this.calcVlaueWidthHalf(value) - this.mValuePaint.getTextSize() / 2);
              let y: number = this.calcYDataResult(entry.getY() * this.scaterChartMode.mAnimator.getPhaseY(), this.mValuePaint.getTextSize() * 2)
              if(dataSet.getValueFormatter()){
                paints = paints.concat(this.drawValue(dataSet.getValueFormatter()!,
                  entry.getY(),
                  entry,
                  i,
                  x,
                  y,
                  dataSet.getValueTextColor(j)))
              }
            }
          } else {
            paints.push(new TextPaint())
          }

          if (entry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

            let icon: ImagePaint | null = entry.getIcon();
            if (icon) {
              let drawIcon: ImagePaint = new ImagePaint(icon);
              let xIcon: number = this.calcXDataResult(entry.getX(), Number(icon.getWidth()) / 2);
              let yIcon: number = this.calcYDataResult(entry.getY(), Number(icon.getHeight()) / 2)
              drawIcon.setX(xIcon + iconsOffset.x)
              drawIcon.setY(yIcon + iconsOffset.y)
              drawIcon.setWidth(Number(icon.getWidth()))
              drawIcon.setHeight(Number(icon.getHeight()))
              paints.push(drawIcon)
            }
          }
        }
        //        MPPointF.recycleInstance(iconsOffset);
      }
    }
    return paints;
  }

  public drawExtras(): Paint[] {
    return [];
  }

  public drawHighlighted(indices: Highlight[]): Paint[] {

    let paints: Paint[] = [];
    if (indices[0].getDataSetIndex() < 0) {
      return paints
    }
    if (indices[0].getDataSetIndex() > this.scaterChartMode.maxVisiableIndex) {
      return paints
    }
    let dataSetArrays = this.scaterChartMode.data.getDataSets();
    if (dataSetArrays) {
      let dataSet = dataSetArrays.get(indices[0].getDataIndex());
      let bubbleEntry: EntryOhos | null = dataSet.getEntryForIndex(indices[0].getDataSetIndex()) as EntryOhos;
      if (bubbleEntry) {
        if (this.scaterChartMode.mAnimator) {
          let x: number = this.calcXDataResult(bubbleEntry.getX(), 0) * this.scaterChartMode.scaleX - 2 + this.scaterChartMode.moveX - this.scaterChartMode.currentXSpace
          let y: number = this.calcYDataResult(bubbleEntry.getY() * this.scaterChartMode.mAnimator.getPhaseY(), 0) * this.scaterChartMode.scaleX - 2 + this.scaterChartMode.moveY - this.scaterChartMode.currentYSpace
          if (this.mHighlightPaint) {
            this.mHighlightPaint.setColor(dataSet.getHighLightColor());
            this.mHighlightPaint.setStrokeWidth(dataSet.getHighlightLineWidth());
            let dashPathEffect = dataSet.getDashPathEffectHighlight();
            if (dashPathEffect) {
              this.mHighlightPaint.setDashPathEffect(dashPathEffect);
            }
          }
          if (dataSet.isVerticalHighlightIndicatorEnabled()) {
            let pathPath = new PathPaint();
            if (this.mHighlightPaint) {
              pathPath.set(this.mHighlightPaint);
            }
            if (this.mViewPortHandler) {
              pathPath.setCommands("M" + vp2px(x) + " " + vp2px(this.mViewPortHandler.contentTop() - this.scaterChartMode.minOffset) + " L" + vp2px(x) + " " + vp2px(this.mViewPortHandler.contentBottom() - this.scaterChartMode.minOffset)) + " Z"
            }
            paints.push(pathPath);
          }
          // draw horizontal highlight lines
          if (dataSet.isHorizontalHighlightIndicatorEnabled()) {

            let pathPath = new PathPaint();
            if (this.mHighlightPaint) {
              pathPath.set(this.mHighlightPaint);
            }
            if (this.mViewPortHandler) {
              pathPath.setCommands("M" + vp2px(this.mViewPortHandler.contentLeft() + this.scaterChartMode.minOffset) + " " + vp2px(y) + " L" + vp2px(this.mViewPortHandler.contentRight() - this.scaterChartMode.minOffset) + " " + vp2px(y)) + " Z"
            }
            // create horizontal path
            paints.push(pathPath)
          }
        }
      }
    }

    return paints;
  }

  public calcXDataResult(x: number, shapeHalf: number): number{
    return x * this.scaterChartMode.xScale + this.scaterChartMode.data.mDisplayRect.left - shapeHalf
  }

  public calcYDataResult(y: number, shapeHalf: number): number{
    return this.scaterChartMode.data.mDisplayRect.bottom - y * this.scaterChartMode.yScale - this.scaterChartMode.data.mDisplayRect.top - shapeHalf
  }

  public calcVlaueWidthHalf(value: string): number{
    if (this.mValuePaint) {
      return value.length * this.mValuePaint.getTextSize() / 2
    }
    return 0;
  }

  public getFormattedValue(value: number): string {
    let str = String(value.toFixed(1)).split(".");
    if (str[1] == "0") {
      return str[0];
    } else {
      return String(value.toFixed(1))
    }
  }
}
