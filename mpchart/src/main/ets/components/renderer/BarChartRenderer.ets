/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import YAxis,{AxisDependency} from '../components/YAxis';
import { BarChartModel }  from '../charts/BarChart';
import IBarDataSet from '../interfaces/datasets/IBarDataSet';
import Range from '../highlight/Range';
import BarLineScatterCandleBubbleRenderer from './BarLineScatterCandleBubbleRenderer';
import MyRect from '../data/Rect';
import BarBuffer from '../buffer/BarBuffer';
import Paint,{Style,RectPaint, ImagePaint, TextPaint} from '../data/Paint';
import BarData from '../data/BarData';
import BarEntry from '../data/BarEntry';
import Transformer from '../utils/Transformer';
import {MyDirection} from '../utils/Fill';
import MPPointF from '../utils/MPPointF';
import ViewPortHandler from '../utils/ViewPortHandler';
import Utils from '../utils/Utils';
import {JArrayList} from '../utils/JArrayList';
import Highlight from '../highlight/Highlight';
import ChartAnimator from '../animation/ChartAnimator'
import EntryOhos from '../data/EntryOhos';

export default class BarChartRenderer extends BarLineScatterCandleBubbleRenderer {

    protected mChart:BarChartModel | null = null;

    /**
     * the rect object that is used for drawing the bars
     */
    protected mBarRect:MyRect = new MyRect();

    protected mBarBuffers:BarBuffer[] = new Array<BarBuffer>();

    protected mShadowPaint:Paint = new RectPaint();
    protected mBarBorderPaint:Paint = new RectPaint();
    private maxTextOffset:number = 0;
    private width:number = 0;
    private height:number = 0;
    private singleWidth:number = 0;
    private marginCount:number  = 12;
    private marginRight:number  = 0;
    private count :number = 0;

    constructor(chart:BarChartModel, viewPortHandler:ViewPortHandler) {
        super(null, viewPortHandler);
        this.mChart = chart;

        this.mHighlightPaint = new Paint();
        this.mHighlightPaint.setStyle(Style.FILL);
        this.mHighlightPaint.setColor(Color.Black);
        // set alpha after color
        this.mHighlightPaint.setAlpha(120);

        this.mShadowPaint.setStyle(Style.FILL);

        this.mBarBorderPaint.setStyle(Style.STROKE);
    }

    public initBuffers():void {
      if (this.mChart) {
        let barData: BarData | null = this.mChart.getBarData();
        if (barData) {
          this.mBarBuffers = new Array(barData.getDataSetCount());
          for (let i = 0; i < this.mBarBuffers.length; i++) {
            let dataSet: IBarDataSet | null = barData.getDataSetByIndex(i);
            if (dataSet) {
              this.mBarBuffers[i] = new BarBuffer(dataSet.getEntryCount() * 4 * (dataSet.isStacked() ? dataSet.getStackSize() : 1),
                barData.getDataSetCount(), dataSet.isStacked());
            }
          }
          this.mAnimator = new ChartAnimator();
        }
      }
    }

    public drawData():Paint[] {
      if (!this.mChart) {
        return []
      }
        this.singleWidth = 0;
        this.marginRight = 0;
        this.count = 0;
        this.width = 0;
        this.height = 0;
        this.maxTextOffset = 0;

        let rectPaintArr:Paint[]=new Array();
        let barData:BarData | null = this.mChart.getBarData();
      if (barData) {
        let leftAxis:YAxis | null = this.mChart.getAxis(AxisDependency.LEFT);
        if (leftAxis) {
          let textPaint:TextPaint = new TextPaint();
          textPaint.setTextSize(leftAxis.getTextSize())
          this.maxTextOffset = Utils.calcTextWidth(textPaint,leftAxis.getAxisMinimum() < 0?leftAxis.getAxisMinimum().toFixed(0)+"":leftAxis.getAxisMaximum().toFixed(0)+"");
        }
        if (this.mViewPortHandler) {
          let right = this.mViewPortHandler.contentRight() - this.maxTextOffset;
          this.width = right - this.mViewPortHandler.contentLeft() - this.maxTextOffset;
          this.height = this.mViewPortHandler.contentBottom() - this.mViewPortHandler.contentTop();
        }

        for(let i = 0; i < barData.getDataSetCount(); i++){
          let dataSet:IBarDataSet | null= barData.getDataSetByIndex(i);
          if (dataSet) {
            if(dataSet.getEntries()){
              this.count += dataSet.getEntries()!.size();
            }
          }
        }
        for (let i = 0; i < barData.getDataSetCount(); i++) {
          let dataSet:IBarDataSet | null= barData.getDataSetByIndex(i);
          if (dataSet) {
            this.singleWidth = this.width / this.count;
            this.marginRight = this.marginCount / this.count;


            if (dataSet.isVisible()) {
              rectPaintArr = rectPaintArr.concat(this.drawDataSet(dataSet, i));
            }
          }
        }
      }

        return rectPaintArr;
    }

    private mBarShadowRectBuffer:MyRect = new MyRect();

  // draw the bar shadow before the values
  drawShadow(rectPaintArr: Paint[], dataSet: IBarDataSet, trans: Transformer | null, phaseX: number): Paint[]{
    if(!this.mChart){
      return [];
    }
    if (this.mChart.isDrawBarShadowEnabled()) {
      if (this.mShadowPaint) {
        this.mShadowPaint.setColor(dataSet.getBarShadowColor());
      }

      let barData:BarData | null= this.mChart.getBarData();
      if (barData) {
        let barWidth:number= barData.getBarWidth();
        let barWidthHalf:number = barWidth / 2.0;
        let x:number =0;
        for (let i = 0, count = Math.min((Math.ceil((dataSet.getEntryCount()) * phaseX)), dataSet.getEntryCount());
          i < count;
          i++) {

          let e :EntryOhos | null= dataSet.getEntryForIndex(i);
          if (e) {
            x = e.getX();

            this.mBarShadowRectBuffer.left = x - barWidthHalf;
            this.mBarShadowRectBuffer.right = x + barWidthHalf;
          }

          if (trans) {
            trans.rectValueToPixel(this.mBarShadowRectBuffer);
          }
          if (this.mViewPortHandler) {
            if (!this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right))
              continue;

            if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left))
              break;

            this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
            this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();
          }

          let rectPaint:RectPaint=new RectPaint(this.mShadowPaint as RectPaint);
          let style = this.mShadowPaint.getStyle();
          if (style) {
            rectPaint.setStyle(style)
          }
          rectPaint.setX(this.mBarShadowRectBuffer.left)
          rectPaint.setY(this.mBarShadowRectBuffer.top)
          rectPaint.setWidth(Math.abs(this.mBarShadowRectBuffer.right-this.mBarShadowRectBuffer.left))
          rectPaint.setHeight(Math.abs(this.mBarShadowRectBuffer.bottom-this.mBarShadowRectBuffer.top))
          //                rectPaint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
          //                rectPaint.setStroke(this.mBarBorderPaint.getColor())
          rectPaintArr.push(rectPaint)
        }
      }
    }
    return rectPaintArr;
  }

  initBuffer(index: number, phaseX: number, phaseY: number, dataSet: IBarDataSet): BarBuffer{
    let buffer: BarBuffer = this.mBarBuffers[index];
    buffer.setPhases(phaseX, phaseY);
    buffer.setDataSet(index);
    buffer.setInverted(this.mChart!.isInverted(dataSet.getAxisDependency()));
    let barDataObj = this.mChart!.getBarData();
    if (barDataObj) {
      buffer.setBarWidth(barDataObj.getBarWidth());
    }

    buffer.feed(dataSet);
    return buffer;
  }

  protected drawDataSet(dataSet: IBarDataSet, index: number): Paint[] {
    if (!this.mChart) {
      return [];
    }
    let trans: Transformer | null = null;
    trans = this.mChart.getTransformer(dataSet.getAxisDependency());
    if (this.mBarBorderPaint) {
      this.mBarBorderPaint.setColor(dataSet.getBarBorderColor());
      this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(dataSet.getBarBorderWidth()));
    }

        let drawBorder:boolean = dataSet.getBarBorderWidth() > 0;

        let phaseX:number = 0;
      if (this.mAnimator) {
        phaseX = this.mAnimator.getPhaseX();
      }

      let phaseY: number = 0
      if (this.mAnimator) {
        phaseY = this.mAnimator.getPhaseY();
      }


        let rectPaintArr:Paint[]=new Array();
        // draw the bar shadow before the values
        rectPaintArr = this.drawShadow(rectPaintArr, dataSet, trans, phaseX)

    // initialize the buffer
    let buffer: BarBuffer = this.initBuffer(index, phaseX, phaseY,dataSet)
      if (trans) {
        trans.pointValuesToPixel(buffer.buffer);
      }


    let isCustomFill: boolean = false;
    let dataFills = dataSet.getFills();
    if (dataFills) {
      isCustomFill = dataFills && !dataFills.isEmpty();
    }
    let isSingleColor: boolean = dataSet.getColors().size() == 1;
    let isInverted: boolean = false;
    isInverted = this.mChart.isInverted(dataSet.getAxisDependency());

        if (isSingleColor && this.mRenderPaint) {
            this.mRenderPaint.setColor(dataSet.getColor());
        }

        let position:number = 0;
        let number = 0;
        let yValPosition = 0;
        for(let i = 0; i < index; i++){
          let barData = this.mChart.getBarData();
          if (barData) {
            let dataSet: IBarDataSet | null = barData.getDataSetByIndex(index - 1);
            if (dataSet) {
              let entries = dataSet.getEntries();
              if (entries) {
                number += entries.size();
              }

            }
          }
        }
      let barDataInstance = this.mChart.getBarData();
      if (barDataInstance){
        if (barDataInstance.getBarWidth() > 0 && barDataInstance.getBarWidth() != null) {

          for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {

            let dataEntries = dataSet.getEntries();
            if (!dataEntries) {
              return []
            }
            let barEntry: BarEntry = dataEntries.get(position) as BarEntry;

            let axis = this.mChart.getAxis(AxisDependency.LEFT)
            if (!axis) {
            return [];
            }
              let yLength = axis.getAxisMaximum() - axis.getAxisMinimum();
              let yAxisMinValueAbs = Math.abs(axis.getAxisMinimum()) / yLength * this.height;
              let positionY = barEntry.getY() > 0 ? 0 : Math.abs(barEntry.getY()) / yLength * this.height

              let xLength = 0;
            let xAxis = this.mChart.getXAxis();
            if (xAxis) {
              xLength = xAxis.getAxisMaximum() - xAxis.getAxisMinimum();
            }

            let xAxisMinValueAbs = 0;
            if (xAxis) {
              xAxisMinValueAbs = Math.abs(xAxis.getAxisMinimum()) / xLength * this.width;
            }

              let positionX = barEntry.getX() / xLength * this.width;

              buffer.buffer[j] = positionX + this.maxTextOffset + this.mChart.finalViewPortHandler.offsetLeft() + xAxisMinValueAbs;
              buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs - (barEntry.getY() > 0 ? (barEntry.getY() / yLength * this.height) : 0);
              buffer.buffer[j + 2] = positionX + this.maxTextOffset + this.mChart.finalViewPortHandler.offsetLeft() + (barDataInstance
                .getBarWidth() / xLength * this.width) + xAxisMinValueAbs;
              buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs + positionY;
              let val : number= barEntry.getY();
            let yVlaues = barEntry.getYVals();

              if (dataSet.isStacked() && yVlaues && yVlaues.length > 0) {
                let height = buffer.buffer[j + 3] - buffer.buffer[j + 1];
                val = yVlaues[yValPosition];
                if (yValPosition == 0) {
                  buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom();
                } else {
                  val = barEntry.getSumBelow(yValPosition);
                  buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                }
                if (yValPosition == yVlaues.length - 1) {
                  yValPosition = 0;
                  position++;
                  number++;
                } else {
                  val = barEntry.getSumBelow(yValPosition + 1);
                  buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                  yValPosition++;
                }
              } else {

                position++;
                number++;
              }


            if (!this.mViewPortHandler || !this.mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2]))
              continue;

            if (!this.mViewPortHandler||!this.mViewPortHandler.isInBoundsRight(buffer.buffer[j]))
              break;

            if (!isSingleColor && this.mRenderPaint) {
              // Set the color for the currently drawn value. If the index
              // is out of bounds, reuse colors.
              this.mRenderPaint.setColor(dataSet.getColor(pos));
            }
            if (this.mChart.cylinderCenter && this.mChart.mXAxis) {
              this.mChart.mXAxis.setXCenterOffset(Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2)
            }
            let paintX = buffer.buffer[j]
            if (this.mChart.cylinderCenter) {
              if (this.mChart.cylinderWidth !== 0) {
                paintX = buffer.buffer[j] + (Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2 - this.mChart.cylinderWidth / 2)
              }
            }
            let paintWidth = (this.mChart.cylinderWidth !== 0) && (this.mChart.cylinderWidth < Math.abs(buffer.buffer[j + 2] - buffer.buffer[j])) ? this.mChart.cylinderWidth : Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]);

            if (isCustomFill) {
              let rectPaint: RectPaint = new RectPaint();
              if (this.mRenderPaint) {
                let style = this.mRenderPaint.getStyle();
                if (style) {
                  rectPaint.setStyle(style)
                }
                rectPaint.setColor(this.mRenderPaint.getColor())
              }
              let dataFill  =  dataSet.getFill(pos);
              if (dataFill) {
                let paint = dataFill.fillRect(
                    rectPaint,
                    buffer.buffer[j],
                    buffer.buffer[j + 1],
                    buffer.buffer[j + 2],
                    buffer.buffer[j + 3],
                    isInverted ? MyDirection.UP : MyDirection.DOWN);
                if (paint){
                  if (paintWidth !== 0 && paintWidth !== Math.abs(buffer.buffer[j + 2] - buffer.buffer[j])) {
                    paint.setWidth(paintWidth)
                  }
                  if (this.mChart.cylinderCenter && paintX !== buffer.buffer[j]) {
                    paint.setX(paintX)
                  }
                  paint.setRadius(this.mChart.radius)
                  paint.setShowBorder(this.mChart.isShowBorder)
                  paint.setStroke(this.mBarBorderPaint.getColor())
                  paint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
                  paint.value = val;
                  rectPaintArr.push(paint);
                }
              }
            } else {
              let rectPaint: RectPaint = new RectPaint();
              if (this.mChart){
                rectPaint.setRadius(this.mChart.radius)
                rectPaint.setShowBorder(this.mChart.isShowBorder)
              }
              if (this.mRenderPaint) {
                let paintStyle = this.mRenderPaint.getStyle();
                if (paintStyle) {
                  rectPaint.setStyle(paintStyle)
                }
                rectPaint.setColor(this.mRenderPaint.getColor())
              }
              rectPaint.setX(buffer.buffer[j])
              rectPaint.setY(buffer.buffer[j + 1])
              rectPaint.setWidth(paintWidth)
              rectPaint.setHeight(Math.abs(buffer.buffer[j + 3] - buffer.buffer[j + 1]))
              if (this.mBarBorderPaint) {
                rectPaint.setStroke(this.mBarBorderPaint.getColor())
                rectPaint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
              }
              rectPaint.value = val;
              rectPaintArr.push(rectPaint)
            }
          }
        } else {
          for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {// 初始化循环计数器 j 和位置 pos
            // 获取数据集的数据点
            let entries  = dataSet.getEntries();
            if (!entries) {
              return [];
            }

            // 获取柱形图数据点
            let barEntry: BarEntry = entries.get(position) as BarEntry;
            if (!barEntry) {
              return [];
            }

            // 获取左侧轴
            let axis =  this.mChart.getAxis(AxisDependency.LEFT);
            if (!axis || !this.mChart) {
              return [];
            }

            // 计算 Y 轴相关参数
            let yLength = axis.getAxisMaximum() - axis.getAxisMinimum();
            let yAxisMinValueAbs = Math.abs(axis.getAxisMinimum()) / yLength * this.height;
            let positionY = barEntry.getY() > 0 ? 0 : Math.abs(barEntry.getY()) / yLength * this.height

            // 设置绘制柱形图的坐标参数
            buffer.buffer[j] = this.singleWidth * number + this.mChart.finalViewPortHandler.offsetLeft() + this.maxTextOffset + this.marginRight
            buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs - (barEntry.getY() > 0 ? (barEntry.getY() / yLength * this.height) : 0);
            buffer.buffer[j + 2] = this.singleWidth * (number + 1) + this.mChart.finalViewPortHandler.offsetLeft() + this.maxTextOffset - this.marginRight
            buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs + positionY;

            // 处理堆叠柱状图的情况
            let val = barEntry.getY();
            let barEntryArray = barEntry.getYVals();
              if (barEntryArray && dataSet.isStacked() && barEntryArray != undefined && barEntryArray.length > 0) {
                let height = buffer.buffer[j + 3] - buffer.buffer[j + 1];
                val = barEntryArray[yValPosition];
                if (yValPosition == 0) {
                  buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom();
                } else {
                  val = barEntry.getSumBelow(yValPosition);
                  buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                }
                if (yValPosition == barEntryArray.length - 1) {
                  yValPosition = 0;
                  position++;
                  number++;
                } else {
                  val = barEntry.getSumBelow(yValPosition + 1);
                  buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                  yValPosition++;
                }
              } else {
                position++;
                number++;
              }

            // 检查是否在视图范围内，如果不在则继续下一次循环
            if (!this.mViewPortHandler || !this.mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2]))
              continue;

            // 检查是否在视图范围内，如果不在则终止循环
            if (!this.mViewPortHandler||!this.mViewPortHandler.isInBoundsRight(buffer.buffer[j]))
              break;

            // 设置当前绘制值的颜色，如果索引超出范围，重用颜色
            if (!isSingleColor && this.mRenderPaint) {
              // Set the color for the currently drawn value. If the index
              // is out of bounds, reuse colors.
              this.mRenderPaint.setColor(dataSet.getColor(pos));
            }

            // 设置绘制柱形图的颜色、中心偏移和宽度
            if (this.mChart.cylinderCenter && this.mChart.mXAxis) {
              this.mChart.mXAxis.setXCenterOffset(Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2)
            }
            let paintX = buffer.buffer[j]
            if (this.mChart.cylinderCenter) {
              if (this.mChart.cylinderWidth !== 0) {
                paintX = buffer.buffer[j] + (Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2 - this.mChart.cylinderWidth / 2)
              }
            }
            let paintWidth = (this.mChart.cylinderWidth !== 0) && (this.mChart.cylinderWidth < Math.abs(buffer.buffer[j + 2] - buffer.buffer[j])) ? this.mChart.cylinderWidth : Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]);

            if (isCustomFill) {
              let rectPaint: RectPaint = new RectPaint();
              let paintStyle = this.mRenderPaint.getStyle();
              if (paintStyle) {
                rectPaint.setStyle(paintStyle)
              }
              rectPaint.setColor(this.mRenderPaint.getColor())
              let dataFill = dataSet.getFill(pos);
              if (dataFill) {
                let paint = dataFill.fillRect(
                    rectPaint,
                    buffer.buffer[j],
                    buffer.buffer[j + 1],
                    buffer.buffer[j + 2],
                    buffer.buffer[j + 3],
                    isInverted ? MyDirection.UP : MyDirection.DOWN);
                if (paint){
                  if (paintWidth !== 0 && paintWidth !== Math.abs(buffer.buffer[j + 2] - buffer.buffer[j])) {
                    paint.setWidth(paintWidth)
                  }
                  if (this.mChart.cylinderCenter && paintX !== buffer.buffer[j]) {
                    paint.setX(paintX)
                  }
                  paint.setRadius(this.mChart.radius)
                  paint.setShowBorder(this.mChart.isShowBorder)
                  paint.setStroke(this.mBarBorderPaint.getColor())
                  paint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
                  paint.value = val;
                  rectPaintArr.push(paint);
                }
              }
            } else {
              let rectPaint: RectPaint = new RectPaint();
              rectPaint.setRadius(this.mChart.radius)
              rectPaint.setShowBorder(this.mChart.isShowBorder)
              let renderPaintStyle = this.mRenderPaint.getStyle();
              if (renderPaintStyle) {
                rectPaint.setStyle(renderPaintStyle)
              }
              rectPaint.setColor(this.mRenderPaint.getColor())
              rectPaint.setX(buffer.buffer[j])
              rectPaint.setY(buffer.buffer[j + 1])
              rectPaint.setWidth(paintWidth)
              rectPaint.setHeight(Math.abs(buffer.buffer[j + 3] - buffer.buffer[j + 1]))
              rectPaint.setStroke(this.mBarBorderPaint.getColor())
              rectPaint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
              rectPaint.value = val;
              rectPaintArr.push(rectPaint)
            }
          }
        }
      }

      return rectPaintArr
    }

    protected  prepareBarHighlight(x:number, y1:number, y2:number, barWidthHalf:number, trans:Transformer):void {

        let left:number = x - barWidthHalf;
        let right:number = x + barWidthHalf;
        let top:number = y1;
        let bottom:number = y2;

        this.mBarRect.set(left, top, right, bottom);

        trans.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
    }

    public drawValues():Paint[] {
        let paintArr:Paint[]=[];
        // if values are drawn
      if (!this.mChart) {
        return []
      }
        if (this.mChart.isDrawingValuesAllowed()) {
            let barData = this.mChart.getBarData();
          if (!barData) {
            return []
          }
            let dataSets:JArrayList<IBarDataSet> = barData.getDataSets();

            let valueOffsetPlus:number = 4.5;
            let posOffset:number = 0;
            let negOffset:number = 0;
            let drawValueAboveBar:boolean = this.mChart.isDrawValueAboveBarEnabled();


            for (let i = 0; i < barData.getDataSetCount(); i++) {

                let dataSet:IBarDataSet | null = dataSets.get(i);
              if (!dataSet) {
                break;
              }
                if (!this.shouldDrawValues(dataSet))
                   continue;

                // apply the text-styling defined by the DataSet
                this.applyValueTextStyle(dataSet);
                // get the buffer
                let buffer:BarBuffer = this.mBarBuffers[i];

                // let phaseY:number = this.mAnimator.getPhaseY();
                let iconsOffset:MPPointF = MPPointF.getInstance(undefined,undefined,dataSet.getIconsOffset());
                if(iconsOffset.x != undefined){
                  iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
                }
                if(iconsOffset.y != undefined){
                  iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);
                }
                let position:number = 0;
                let number = 0;
                let yValPosition: number = 0;
                for(let j = 0; j < i; j++){

                  let dataSet:IBarDataSet | null = barData.getDataSetByIndex(i - 1);
                  if (dataSet) {
                    let entries = dataSet.getEntries();
                    if (entries) {
                      number += entries.size();
                    }
                  }
                }
                for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {
                  let entries = dataSet.getEntries();
                  if (entries) {
                    let barEntry:BarEntry = entries.get(position) as BarEntry;

                    let yLength:number =0;
                    let axisValue = this.mChart.getAxis(AxisDependency.LEFT);
                    if (axisValue) {
                      yLength= axisValue.getAxisMaximum() - axisValue.getAxisMinimum();
                    }
                    let yAxisMinValueAbs:number = 0;
                    if (axisValue) {
                      yAxisMinValueAbs= Math.abs(axisValue.getAxisMinimum()) / yLength  * this.height;
                    }
                    let positionY = barEntry.getY() > 0 ? 0 : Math.abs(barEntry.getY()) / yLength  * this.height
                    if (this.mViewPortHandler) {
                      buffer.buffer[j] = this.singleWidth * number + this.mViewPortHandler.offsetLeft() + this.maxTextOffset + this.marginRight
                      buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs - (barEntry.getY() > 0 ? (barEntry.getY() / yLength * this.height) : -positionY);
                      buffer.buffer[j + 2] = this.singleWidth * (number + 1) + this.mViewPortHandler.offsetLeft() + this.maxTextOffset - this.marginRight
                      buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs + positionY;
                    }
                    if(dataSet.isStacked()){
                      let height = buffer.buffer[j + 3] - buffer.buffer[j + 1];
                      let yValues = barEntry.getYVals();
                      if (!yValues) {
                        return []
                      }
                      let val = yValues[yValPosition];
                      if(yValPosition == 0){
                        buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom();
                      }else{
                        val = barEntry.getSumBelow(yValPosition);
                        buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                      }
                      if(yValPosition == yValues.length - 1){
                        yValPosition = 0;
                        position++;
                        number++;
                      }else{
                        val = barEntry.getSumBelow(yValPosition + 1);
                        buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                        yValPosition++;
                      }
                    }else{

                      position++;
                      number++;
                    }
                    let x:number = (buffer.buffer[j] + buffer.buffer[j + 2]) / 2;

            let val: number = barEntry.getY();
            let axis = this.mChart.getAxis(AxisDependency.LEFT);
            if (axis) {
              if (Utils.calcTextWidth(this.mValuePaint, String(axis.getAxisMaximum().toFixed(1))
                .replace(".", "")) > (this.singleWidth * 1.5)) {
                this.mValuePaint.setTextSize(Utils.convertDpToPixel(this.singleWidth / String(axis.getAxisMaximum()
                  .toFixed(1)).replace(".", "").length));
              }
            }
            let isInverted: boolean = this.mChart.isInverted(dataSet.getAxisDependency());
            // calculate the correct offset depending on the draw position of
            // the value
            let valueTextWidth = Utils.calcTextWidth(this.mValuePaint, String(val.toFixed(1)));
            let valueTextHeight: number = Utils.calcTextHeight(this.mValuePaint, "8");
            posOffset = (drawValueAboveBar ? -valueOffsetPlus : valueTextHeight + valueOffsetPlus);
            negOffset = (drawValueAboveBar ? valueTextHeight + valueOffsetPlus : -valueOffsetPlus);
            if (isInverted) {
              posOffset = -posOffset - valueTextHeight;
              negOffset = -negOffset - valueTextHeight;
            }

                    posOffset = (this.singleWidth / 2) - valueTextWidth / 2;
                    if(dataSet.isStacked()){
                      let yValues = barEntry.getYVals();
                      if (yValues) {
                        val = yValues[yValPosition]
                      }

                      negOffset -= valueTextHeight + valueOffsetPlus;
                      dataSet.setValueTextColor(0xffffff)
                    }
                    if (dataSet.isDrawValuesEnabled()) {
                      if(dataSet.getValueFormatter()){
                        paintArr = paintArr.concat(this.drawValue(dataSet.getValueFormatter()!, val, barEntry, i, buffer.buffer[j] + posOffset,
                          buffer.buffer[j + 1] - (barEntry.getY() > 0 ? negOffset : -valueOffsetPlus),
                          dataSet.getValueTextColor(j / 4)));
                      }
                    }

                    if (barEntry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

                      let icon:ImagePaint | null= barEntry.getIcon();
                      if (icon) {
                        let px:number = x;
                        let py:number = val >= 0 ?
                          (buffer.buffer[j + 1] + posOffset) :
                          (buffer.buffer[j + 3] + negOffset);

                        px += iconsOffset.x;
                        py += iconsOffset.y;

                        paintArr = paintArr.concat(Utils.drawImage(
                          icon.getIcon(),
                          px,
                          py,
                          Number(icon.getWidth()),
                          Number(icon.getHeight())));
                      }
                    }
                  }


                  // MPPointF.recycleInstance(iconsOffset);
                  }
            }
        }
        return paintArr;
    }

    public drawHighlighted(indices:Highlight[]):Paint[] {
      if(!this.mChart){
        return [];
      }
        let rectPaintArr:RectPaint[]=new Array();

        let barData:BarData |null = this.mChart.getBarData();
      if (barData) {
        for (let high of indices) {

          let dataSet:IBarDataSet | null = barData.getDataSetByIndex(high.getDataSetIndex());

          if (dataSet == null || !dataSet.isHighlightEnabled())
            continue;

          let e = dataSet.getEntryForXValue(high.getX(), high.getY());
          if (e) {
            if (!this.isInBoundsX(e, dataSet))
              continue;

            let trans:Transformer | null = this.mChart.getTransformer(dataSet.getAxisDependency());

            this.mHighlightPaint.setColor(dataSet.getHighLightColor());
            this.mHighlightPaint.setAlpha(dataSet.getHighLightAlpha());

            let isStack:boolean = (high.getStackIndex() >= 0  && (e as BarEntry).isStacked()) ? true : false;

            let y1:number = 0;
            let y2:number = 0;

            if (isStack) {

              if(this.mChart.isHighlightFullBarEnabled()) {

                y1 = (e as BarEntry).getPositiveSum();
                y2 = -(e as BarEntry).getNegativeSum();

              } else {
                let rangeArray = (e as BarEntry).getRanges();
                if (rangeArray) {
                  let range:Range = rangeArray[high.getStackIndex()];

                  y1 = range.myfrom;
                  y2 = range.to;
                }
              }
            } else {
              y1 = e.getY();
              y2 = 0.;
            }
            if (trans) {
              this.prepareBarHighlight(e.getX(), y1, y2, barData.getBarWidth() / 2, trans);
            }

            this.setHighlightDrawPos(high, this.mBarRect);
            let rectPaint:RectPaint=new RectPaint();
            let style = this.mHighlightPaint.getStyle();
            if (style) {
              rectPaint.setStyle(style);
            }
            rectPaint.setStrokeWidth(this.mHighlightPaint.getStrokeWidth());
            rectPaint.setColor(this.mHighlightPaint.getColor());
            rectPaint.setAlpha(this.mHighlightPaint.getAlpha());
            rectPaint.setX(this.mBarRect.left)
            rectPaint.setY(this.mBarRect.top)
            rectPaint.setWidth(this.mBarRect.right-this.mBarRect.left)
            rectPaint.setHeight(this.mBarRect.bottom-this.mBarRect.top)
            rectPaintArr.push(rectPaint);
          }
        }
      }

        return rectPaintArr
    }

    /**
     * Sets the drawing position of the highlight object based on the riven bar-rect.
     * @param high
     */
    protected  setHighlightDrawPos( high:Highlight,  bar:MyRect):void {
        high.setDraw(bar.centerX(), bar.top);
    }

    public drawExtras():Paint[] | null {
        return null;
    }

    public getMarginRight(): number{
      return this.marginRight;
    }

    public drawClicked(paint:Paint,paint2:TextPaint,count: number,position: number): Paint[]{
      if (!this.mChart) {
        return [];
      }
      let rectPaint = new RectPaint();
      rectPaint.set(paint);
      rectPaint.setGradientFillColor([[0x80000000,1],[0x80000000,1]]);
      rectPaint.setClickPosition(position)
      let barData  = this.mChart.getBarData()
      if (!barData) {
        return []
      }
      let dataSets:JArrayList<IBarDataSet> = barData.getDataSets();
      let dataSet:IBarDataSet = dataSets.get(0);
      if(!dataSet.isDrawValuesEnabled() || dataSet.isStacked() || !this.mChart.mDrawClickText){
        return [rectPaint];
      }

      let textPaint = new TextPaint(paint2);

      let xValue : number = 0;
      let xAxis = this.mChart.getXAxis();
      if (xAxis) {
        xValue =  xAxis.getAxisMaximum() / count * (position + 1);
      }

      textPaint.setText("x:"+xValue+",y:"+textPaint.text)
      textPaint.setX((paint.x + Number(paint.width) / 2) - (Utils.calcTextWidth(this.mValuePaint,textPaint.text) / 2));
      let imagePaint = new ImagePaint();
      imagePaint.setWidth(Utils.calcTextWidth(textPaint,textPaint.text) + 20);
      imagePaint.setHeight(Utils.calcTextHeight(textPaint,textPaint.text) * 4);
      imagePaint.setIcon("app.media.marker2")
      let imageOffsetY = (Utils.calcTextHeight(textPaint,textPaint.text) / 2) + (Number(imagePaint.getHeight()) / 2);
      imagePaint.setX((paint.x + Number(paint.width) / 2) - (Number(imagePaint.width) / 2));
      imagePaint.setY(paint2.getY() - imageOffsetY);
      textPaint.setY(imagePaint.y + (Number(imagePaint.height) / 2) - Utils.calcTextHeight(textPaint,textPaint.text.replace(".","")))
      textPaint.setColor("#ffffff");
      return [rectPaint,imagePaint,textPaint];
    }

}
