/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { LineChartModel } from '../charts/LineChart';
import ScaleMode from '../data/ScaleMode';
import MyRect from '../data/Rect';
import Paint from '../data/Paint';
import { XAxis } from './XAxis';
import YAxis from './YAxis';
import {PathPaint} from '../data/Paint';
import { CirclePaint , PathFillPaint,TextPaint, LinePaint, ImagePaint}  from '../data/Paint'
import LineChartRenderer from '../renderer/LineChartRenderer';
import LineData from '../data/LineData';
import Utils from '../utils/Utils'

@Component
export default struct PathView {
  @ObjectLink model:PathViewModel

  public aboutToAppear() {
    this.model.calcClipPath();
  }
  public test():boolean{
    console.log("pathview:"+this.model.mWidth)
    return true
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      if(this.test()){}
      if (this.model.paintArry.length > 0) {
        ForEach(this.model.paintArry, (item: Paint) => {

          if (item instanceof PathPaint) {
            Shape() {
              Path()
                .commands(item.commands)
                .strokeWidth(item.strokeWidth)
                .stroke(item.stroke)
                .strokeDashArray(item.strokeDashArray)
                .strokeDashOffset(item.strokeDashOffset)
            }
            .fill('none')
          } else if (item instanceof PathFillPaint) {
            Shape() {
              if (item.isShowFillLine) {
                Path()
                  .commands(item.commandsFill)
              }
            }
            .width(this.model.mWidth)
            .height(this.model.mHeight)
            .fill('none')
            .linearGradient({ colors: item.linearGradientColors })
            .clip(new Path().commands(item.commandsFill))
          } else if (item instanceof TextPaint) {

            Text(item.text)
              .fontSize(8)
              .fontColor(item.color)
              .position({ x: item.getX(), y: item.getY() })
          } else if (item instanceof CirclePaint) {
            if (item.isDrawCirclesEnabled()) {
              Circle({ width: item.radius * 2, height: item.radius * 2 })
                .position({ x: item.getX() - item.radius, y: item.getY() - item.radius })
                .fill(item.colors != null ? item.colors.at(0) : item.circleColor)
                .stroke(Color.Transparent)
              if (item.circleHoleEnabled) {
                Circle({ width: item.circleHoleRadius * 2, height: item.circleHoleRadius * 2 })
                  .position({ x: item.getX() - item.circleHoleRadius, y: item.getY() - item.circleHoleRadius })
                  .fill(item.circleHoleColor)
                  .stroke(Color.Transparent)
              }
            }
          }
        }, (item: Paint) => JSON.stringify(item))
        if(this.model.heightlightPaint.length > 0){
          ForEach(this.model.heightlightPaint, (item: Paint) => {
            if(item instanceof LinePaint){
              Line()
                .width(this.model.mWidth)
                .height(this.model.mHeight)
                .startPoint(item.startPoint)
                .endPoint(item.endPoint)
                .fill(item.color)
                .stroke(item.color)
                .strokeWidth(item.strokeWidth)
                .position({ x: 0, y: 0 })
                .visibility(this.model.isShowHeightlight ? Visibility.Visible : Visibility.None)
            } else if (item instanceof ImagePaint){
              Image($r('app.media.marker2'))
                .width(item.width)
                .height(item.height)
                .objectFit(ImageFit.Fill)
                .position({ x:item.x,y:item.y })
                .visibility(this.model.isShowClickValue ? Visibility.Visible : Visibility.None)
            } else if (item instanceof TextPaint){
              Text(item.text)
                .position({ x: item.x, y: item.y })
                .fontSize(item.textSize)
                .textAlign(item.textAlign)
                .fontColor(item.color)
                .visibility(this.model.isShowClickValue ? Visibility.Visible : Visibility.None)
            }
          }, (item: Paint) => JSON.stringify(item))
        }
      }
    }
    .width(this.model.mWidth)
    .height(this.model.mHeight)
    .backgroundColor(this.model.backgroundColor)
    .clip(new Path().commands(this.model.clipPath))
    .position({ x: this.model.positionX, y: Math.max(this.model.minOffset,this.model.topMinOffset)})
    .onClick((event: ClickEvent | undefined)=>{
      if (event) {
        this.model.onClick(event);
      }
    })
    .onTouch((event: TouchEvent | undefined) => {
      if (event) {
        this.model.onTouch(event)
      }
    })
  }

}
@Observed
export class PathViewModel extends ScaleMode{
  yleftAxis: YAxis = new YAxis();
  yRightAxis: YAxis = new YAxis();
  xAxis: XAxis = new XAxis();
  mWidth: number = 300;
  mHeight: number = 300;
  isInverted: boolean = false;
  isShowHeightlight: boolean = true;
  isShowClickValue: boolean = true;
  minOffset: number = 15; //X轴线偏移量
  topMinOffset: number = 0;
  bottomMinOffset: number = 0;
  positionX: number= 0;
  lineData: LineData = new LineData()
  xScale: number = 1;
  yLeftScale: number = 1;
  yRightScale: number = 1;
  backgroundColor: Color | string | number = "#00FFFFFF"; // chart区域背景色 默认透明色
  rect: MyRect = new MyRect();
  paintArry: Paint[] = [];
  valuePaint: Paint[] = [];
  heightlightPaint: Paint[] = [];
  mRenderer: LineChartRenderer | null = null;
  eventX: number = 0;
  eventY: number = 0;
  clickTransX: number = 0;
  clickTransY: number = 0;
  clipPath: string = "";
  lineChartModel: LineChartModel | null =null;

  public setYLeftAxis(yleftAxis: YAxis): void{
      this.yleftAxis = yleftAxis;
  }

  public setYRightAxis(yRightAxis: YAxis): void{
    this.yRightAxis = yRightAxis;
  }

  public setXAxis(xAxis: XAxis): void{
    this.xAxis = xAxis;
  }

  public setIsInverted(isInverted: boolean): void{
    this.isInverted = isInverted;
  }

  public setIsShowHeightlight(isShowHeightlight: boolean): void{
    this.isShowHeightlight = isShowHeightlight;
  }

  public setIsShowClickValue(isShowClickValue: boolean): void{
    this.isShowClickValue = isShowClickValue;
  }

  public getLineData(): LineData{
    return this.lineData;
  }

  public setPathViewData(lineData: LineData): void{
    this.lineData = lineData;

    this.computerScale();
    if (this.yleftAxis && this.yRightAxis) {
      this.mRenderer = new LineChartRenderer(this, this.yleftAxis, this.yRightAxis, this.isInverted);
    }
   

    this.ondraw();
  }

  private computerScale(): void{
    this.rect = this.lineData.mDisplayRect;

    this.positionX = this.rect.left;
    if (this.xAxis) {
      this.xScale = (this.rect.right - this.rect.left)/(this.xAxis.getAxisMaximum()-this.xAxis.getAxisMinimum());
    }
    if (this.yleftAxis) {
      this.yLeftScale = (this.rect.bottom - this.rect.top)/(this.yleftAxis.getAxisMaximum()-this.yleftAxis.getAxisMinimum());
    }

    if (this.yRightAxis) {
      this.yRightScale = (this.rect.bottom - this.rect.top)/(this.yRightAxis.getAxisMaximum()-this.yRightAxis.getAxisMinimum());
    }

  }

  private ondraw(){
    if (this.mRenderer) {
      let pathPaint: Paint[] = this.mRenderer.drawData();
      let circlePaint: Paint[] = this.mRenderer.drawCircle();
      this.valuePaint = this.mRenderer.drawValues();
      this.heightlightPaint = this.mRenderer.drawHighlighted();
      this.paintArry = [];
      this.paintArry = this.paintArry.concat(pathPaint);
      this.paintArry = this.paintArry.concat(circlePaint);
      this.paintArry = this.paintArry.concat(this.valuePaint);
    }
  }
  public onSingleClick(event: ClickEvent){
    this.eventX = event.x;
    this.eventY = event.y;
//    console.log("jk: event.x = "+event.x+", event.screenX = "+event.screenX)
//    let screenCenterX: number = (this.rect.right - this.rect.left) / 2;
//    let screenCenterY: number = (this.rect.bottom - this.rect.top) / 2;
//    console.log("jk: this.eventY = "+this.eventY+", screenCenterY = "+screenCenterY);

//    if(this.scaleX > 1){
//      this.clickTransX = screenCenterX - this.eventX;
//      this.clickTransY = screenCenterY - this.eventY;
//      this.ondraw();
//    }
    this.heightlightPaint = [];
    if (this.mRenderer) {
      this.heightlightPaint = this.mRenderer.drawHighlighted();
    }
  }

  transX: number = 0;
  transMaxY: number = 0;
  transMinY: number = 0;
  public onDoubleClick(event: ClickEvent){
    this.currentXSpace =  this.centerX * this.scaleX - this.centerX;
    this.currentYSpace = this.centerY * this.scaleY - this.centerY;

    if (this.paintArry === null || this.paintArry === undefined || this.paintArry.length === 0) {
      return
    }

    let maxXBefore: number = this.paintArry[this.paintArry.length - 1].x;
    let maxYBefore: number = Number.MIN_VALUE;
    let minYBefore: number = Number.MAX_VALUE;

    for(let i = 0; i < this.valuePaint.length; i++){
      if(maxYBefore < this.valuePaint[i].y){
        maxYBefore = this.valuePaint[i].y;
      }
      if(minYBefore > this.valuePaint[i].y){
        minYBefore = this.valuePaint[i].y;
      }
    }

    this.ondraw();

    let maxXAfter: number = this.paintArry[this.paintArry.length - 1].x;
    let maxYAfter: number = Number.MIN_VALUE;
    let minYAfter: number = Number.MAX_VALUE;

    for(let i = 0; i < this.valuePaint.length; i++){
      if(maxYAfter < this.valuePaint[i].y){
        maxYAfter = this.valuePaint[i].y;
      }
      if(minYAfter > this.valuePaint[i].y){
        minYAfter = this.valuePaint[i].y;
      }
    }

    this.transX += maxXAfter - maxXBefore;
    this.transMaxY += maxYAfter - maxYBefore;
    this.transMinY += minYAfter - minYBefore;
    if (this.lineChartModel != null) {
      this.lineChartModel.test = "" + this.currentYSpace;
    }
    this.setXPosition(this.moveX-this.currentXSpace)
    let moveYSource=this.leftAxisModel.lastHeight*this.scaleY - this.leftAxisModel.lastHeight
    this.leftAxisModel.translate(moveYSource+this.moveY-this.currentYSpace);

  }

  public onMove(event: TouchEvent) {
    let finalMoveX = this.currentXSpace - this.moveX
    let finalMoveY = this.currentYSpace - this.moveY
    if (this.moveX > 0 && finalMoveX <= 0) {
      this.moveX = this.currentXSpace
    }
    if (this.moveY > 0 && finalMoveY <= 0) {
      this.moveY = this.currentYSpace
    }

    if (this.moveX - this.currentXSpace <= this.mWidth - this.xAixsMode.mWidth) {
      this.moveX = this.mWidth - this.xAixsMode.mWidth + this.currentXSpace
    }
    let scaleYHeight = this.mHeight * this.scaleY
    if (this.moveY - this.currentYSpace <= this.mHeight - scaleYHeight) {
      this.moveY = this.mHeight - scaleYHeight + this.currentYSpace
    }


    let moveYSource = this.leftAxisModel.mHeight - this.leftAxisModel.lastHeight
    this.leftAxisModel.translate(moveYSource + this.moveY - this.currentYSpace);
    this.rightAxisModel.translate(moveYSource + this.moveY - this.currentYSpace);
    if (this.lineChartModel != null) {
      this.lineChartModel.test = "" + this.moveY;
    }
    this.setXPosition(this.moveX - this.currentXSpace)
    this.ondraw();
  }

  public onScale(event: TouchEvent){
    this.currentXSpace =  this.centerX * this.scaleX - this.centerX;
    this.currentYSpace = this.centerY * this.scaleY - this.centerY;

    this.ondraw();
//
//    if (this.lineChartModel != null) {
//      this.lineChartModel.test = "" + this.currentYSpace;
//    }
    this.setXPosition(this.moveX-this.currentXSpace)
    let moveYSource=this.leftAxisModel.lastHeight*this.scaleY - this.leftAxisModel.lastHeight
    this.leftAxisModel.translate(moveYSource+this.moveY-this.currentYSpace);
  }

  public calcClipPath(){
    if (!!!this.rect) {
      return
    }
    this.clipPath = 'M' + Utils.convertDpToPixel(this.rect.left - this.positionX) + ' ' + Utils.convertDpToPixel(this.rect.top - this.topMinOffset)
      + 'L' + Utils.convertDpToPixel(this.rect.right - this.positionX) + ' ' + Utils.convertDpToPixel(this.rect.top - this.topMinOffset)
      + 'L' + Utils.convertDpToPixel(this.rect.right - this.positionX) + ' ' + Utils.convertDpToPixel(this.rect.bottom - this.topMinOffset)
      + 'L' + Utils.convertDpToPixel(this.rect.left - this.positionX) + ' ' + Utils.convertDpToPixel(this.rect.bottom - this.topMinOffset)
      + ' Z'
  }

  public setBackgroundColor(color: Color | string | number): void{
    this.backgroundColor = color;
  }

  public setMinOffset(minOffset: number): void{
    this.minOffset = minOffset;
  }

  public setTopMinOffset(topMinOffset: number): void{
    this.topMinOffset = topMinOffset;
  }

  public setBottomMinOffset(bottomMinOffset: number): void{
    this.bottomMinOffset = bottomMinOffset;
  }

  public getYLeftAxis(): YAxis | null{
    return this.yleftAxis;
  }

  public getYRightAxis(): YAxis | null{
    return this.yRightAxis;
  }

  public getXAxis(): XAxis | null{
    return this.xAxis;
  }

  // ================================== 动画相关 ======================================

  private timerXId: number = -1;
  private timerYId: number = -1;
  private timerXyId: number = -1;
  private curX: number = 0;
  private curY: number = 0;
  private pathPaint: Paint[] = [];
  private circlePaint: Paint[] = [];
  private textPaint: Paint[] = [];
  private tempArry: Paint[] = [];

  public animateX(durationMillis: number){
    clearInterval(this.timerXId);
    clearInterval(this.timerYId);
    clearInterval(this.timerXyId)
    this.curX = 0;
    this.clearPaint();
    let maxCountEntrySet = this.lineData.getMaxEntryCountSet();
    if (maxCountEntrySet) {
      let maxCount = maxCountEntrySet.getEntryCount();

      if (this.mRenderer){
        this.mRenderer.animateY(1)
      }
      this.timerXId = setInterval(() => {
        this.clearPaint();
        if (this.mRenderer) {
          this.mRenderer.animateX(this.curX++);
        }
        this.onDraw();

        if(this.curX >= maxCount){
          this.curX = 0;
          clearInterval(this.timerXId)
        }
      },durationMillis)
    }
   
  }

  public animateY(durationMillis: number){
    clearInterval(this.timerXId);
    clearInterval(this.timerYId);
    clearInterval(this.timerXyId)
    this.curY = 0;
    this.clearPaint();
    let maxCountEntrySet = this.lineData.getMaxEntryCountSet();
    if (maxCountEntrySet) {
      let maxCount = maxCountEntrySet.getEntryCount();
      if (this.mRenderer) {
        this.mRenderer.animateX(maxCount);
      }

      this.timerYId = setInterval(() => {
        this.clearPaint();
        if (this.mRenderer) {
          this.mRenderer.animateY(this.curY += 1 / maxCount);
        }

        this.onDraw();

        if(this.curY >= 1){
          this.curY = 0;
          clearInterval(this.timerYId)
        }
      },durationMillis)
    }
    
  }

  public animateXY(durationMillis: number){
    clearInterval(this.timerXId);
    clearInterval(this.timerYId);
    clearInterval(this.timerXyId)
    this.curX = 0;
    this.curY = 0;
    this.clearPaint();
    let maxCountEntrySet = this.lineData.getMaxEntryCountSet();
    if (maxCountEntrySet) {
      let maxCount = maxCountEntrySet.getEntryCount();

      this.timerXyId = setInterval(() => {
        this.clearPaint();
        if (this.mRenderer) {
          this.mRenderer.animateX(this.curX++);
          this.mRenderer.animateY(this.curY += 1 / maxCount);
        }

        this.onDraw();

        if(this.curY >= 1){
          this.curX = 0;
          this.curY = 0;
          clearInterval(this.timerXyId)
        }
      },durationMillis)
    }
  }

  private clearPaint(){
    this.pathPaint = [];
    this.circlePaint = [];
    this.textPaint = [];
    this.tempArry = [];
    this.paintArry = [];
  }

  private onDraw(){
    if (this.mRenderer) {
      this.pathPaint = this.mRenderer.drawData();
      this.circlePaint = this.mRenderer.drawCircle();
      this.textPaint = this.mRenderer.drawValues();
      this.heightlightPaint = this.mRenderer.drawHighlighted();

      this.tempArry = this.tempArry.concat(this.pathPaint);
      this.tempArry = this.tempArry.concat(this.circlePaint);
      this.tempArry = this.tempArry.concat(this.textPaint);

      this.paintArry = this.paintArry.concat(this.tempArry);

      if (this.lineChartModel != null) {
        this.lineChartModel.test = "" + this.curY+ this.curX;
      }
    }
  }
}

