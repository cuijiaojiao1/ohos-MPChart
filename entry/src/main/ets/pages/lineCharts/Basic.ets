/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { LineChart, LineChartModel } from '@ohos/mpchart'
import { XAxis, XAxisPosition } from '@ohos/mpchart'
import { YAxis, AxisDependency, YAxisLabelPosition } from '@ohos/mpchart'
import { LineData } from '@ohos/mpchart'
import { LineDataSet, ColorStop, Mode } from '@ohos/mpchart'
import { EntryOhos } from '@ohos/mpchart'
import { JArrayList } from '@ohos/mpchart'
import { ILineDataSet } from '@ohos/mpchart'
import { LimitLine, LimitLabelPosition } from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct Basic {
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Animate X', 'Animate Y', 'Animate XY'];
  //标题栏标题
  private title: string = 'LineChartActivity 1'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 30; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  lineData: LineData | null = null;
  @State
  lineChartModel: LineChartModel = new LineChartModel();
  titleSelcetString: string = 'X'

  //标题栏菜单回调
  menuCallback() {

    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if (index == undefined || index == -1) {
      return
    }
    switch (this.menuItemArr[index]) {
      case 'View on GitHub':
      //TODO View on GitHub
        break;
      case 'Toggle Values':
        break;
      case 'Toggle Icons':
        break;
      case 'Toggle Filled':
        break;
      case 'Toggle Highlight':
        break;
      case 'Toggle Highlight Circle':
        break;
      case 'Toggle Rotation':
        break;
      case 'Toggle X-Values':
        break;
      case 'Spin Animation':
        break;
      case 'Animate X':
        this.titleSelcetString = 'X'
        this.animate()
        break;
      case 'Animate Y':
        this.titleSelcetString = 'Y'
        this.animate()
        break;
      case 'Animate XY':
        this.titleSelcetString = 'XY'
        this.animate()
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public animate() {
    if (this.titleSelcetString == 'X') {
      this.lineChartModel.pathViewModel.animateX(60);
    } else if (this.titleSelcetString == 'Y') {
      this.lineChartModel.pathViewModel.animateY(60);
    } else if (this.titleSelcetString == 'XY') {
      this.lineChartModel.pathViewModel.animateXY(60);
    }

  }

  public aboutToAppear() {
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title

    this.lineData = this.initCurveData(30, 180);

    this.topAxis.setLabelCount(4, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(11);
    this.topAxis.setAxisMaximum(44);

    this.bottomAxis.setLabelCount(4, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(11);
    this.bottomAxis.setAxisMaximum(44);
    this.bottomAxis.setDrawAxisLine(false);
    this.bottomAxis.setDrawLabels(false)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(10, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(10);
    this.leftAxis.setAxisMaximum(200);
    this.leftAxis.enableGridDashedLine(5, 5, 0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(10, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(10); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(200);
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false);

    let upperLimtLine: LimitLine = new LimitLine(150, "Upper Limit");
    upperLimtLine.setLineWidth(4);
    upperLimtLine.enableDashedLine(5, 5, 0);
    upperLimtLine.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
    upperLimtLine.setTextSize(10);

    let lowerLimtLine: LimitLine = new LimitLine(-30, "Lower Limit");
    lowerLimtLine.setLineWidth(4);
    lowerLimtLine.enableDashedLine(5, 5, 0);
    lowerLimtLine.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
    lowerLimtLine.setTextSize(10);

    this.leftAxis.setDrawLimitLinesBehindData(true);
    this.leftAxis.addLimitLine(upperLimtLine);
    this.leftAxis.addLimitLine(lowerLimtLine);

    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    if (this.leftAxis) {
      this.lineChartModel.setLeftAxis(this.leftAxis);
    }
    if (this.rightAxis) {
      this.lineChartModel.setRightAxis(this.rightAxis);
    }
    if (this.lineData) {
      this.lineChartModel.setLineData(this.lineData);
    }
    this.lineChartModel.init();
  }

  /**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let values = new JArrayList<EntryOhos>();

    for (let i = 0; i < count; i++) {
      let val: number = Math.random() * range;
      values.add(new EntryOhos(i, val));
    }

    let gradientFillColor = new Array<ColorStop>();
    gradientFillColor.push(['#ffff0000', 0.1])
    gradientFillColor.push(['#00ff0000', 1.0])

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(values, "DataSet 1");
    set1.setDrawFilled(true);
    set1.setShowFillLine(true)
    set1.setDrawValues(true);
    set1.enableDashedLine(10, 5, 0)
    set1.setMode(Mode.LINEAR);
    set1.setGradientFillColor(gradientFillColor)
    set1.setColorByColor(Color.Black);
    set1.setLineWidth(1)
    set1.setDrawCircles(true);
    set1.setCircleColor(Color.Black);
    set1.setCircleRadius(2);
    set1.setCircleHoleRadius(1)
    set1.setCircleHoleColor(Color.Green)
    set1.setDrawCircleHole(false)
    dataSet.add(set1);

    return new LineData(dataSet)
  }

  private changeData() {
    this.lineChartModel.setLineData(this.initCurveData(30, 180));
    this.lineChartModel.invalidate();
  }

  build() {
    Column() {
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }) {
        LineChart({ lineChartModel: $lineChartModel })
      }

      Button("切换数据").onClick(() => {
        this.changeData();
      }).margin(30)
    }
  }
}