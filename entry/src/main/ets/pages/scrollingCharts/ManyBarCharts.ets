/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {XAxis, XAxisPosition} from '@ohos/mpchart'
import {YAxis, AxisDependency, YAxisLabelPosition} from '@ohos/mpchart'
import { Fill } from '@ohos/mpchart'
import {ImagePaint} from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart'
import {JArrayList} from '@ohos/mpchart'
import { BarDataSet } from '@ohos/mpchart'
import { BarData } from '@ohos/mpchart'
import {BarChart, BarChartModel} from '@ohos/mpchart'
import { IBarDataSet } from '@ohos/mpchart'
import { MultipleLegend } from '@ohos/mpchart'
import { LegendEntry } from '@ohos/mpchart'
import { ColorTemplate } from '@ohos/mpchart'

interface valueItem{
  index: number;
  data: BarChartModel
}
@Entry
@Component
struct ManyBarCharts {
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  private list: Array<BarChartModel>  = new Array<BarChartModel>();
  private xList: Array<XAxis>  = new Array<XAxis>();
  private YLeftList: Array<YAxis>  = new Array<YAxis>();
  private YRightList: Array<YAxis>  = new Array<YAxis>();
  private legendLineList: Array<LegendEntry> = new Array<LegendEntry>();

  aboutToAppear() {
    // 20 items
    for (let i = 0; i < 20; i++) {
      this.creatAxis();
      this.setData(i + 1, this.creatModel(this.xList[i], this.YLeftList[i], this.YRightList[i]), this.xList[i], this.YLeftList[i], this.YRightList[i])
      this.setLineLengend(i + 1);
    }
  }

  public setLineLengend(i:number) {
    let legendLine: LegendEntry= new LegendEntry();
    legendLine.colorWidth = 10;
    legendLine.colorHeight = 10;
    legendLine.colorItemSpace = 3
    legendLine.colorLabelSpace = 4
    legendLine.labelColor = Color.Black
    legendLine.labelTextSize = 10

    legendLine.colorArr = ColorTemplate.VORDIPLOM_COLORS;
    legendLine.label = "New DataSet "+ i;
    this.legendLineList.push(legendLine);
  }

  private creatAxis() {
    let leftAxis = new YAxis(AxisDependency.LEFT);
    leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    leftAxis.setDrawGridLines(true);
    leftAxis.setAxisMinimum(0);
    leftAxis.setAxisMaximum(100);

    let rightAxis = new YAxis(AxisDependency.RIGHT);
    rightAxis.setDrawGridLines(false);
    rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    rightAxis.setAxisMaximum(100);

    let bottomAxis: XAxis = new XAxis();
    bottomAxis.setLabelCount(6, false);
    bottomAxis.setPosition(XAxisPosition.BOTTOM);
    bottomAxis.setAxisMinimum(0);
    bottomAxis.setAxisMaximum(12);

    this.xList.push(bottomAxis);
    this.YLeftList.push(leftAxis);
    this.YRightList.push(rightAxis);
  }

  private creatModel(xAxis: XAxis, yleftAxis: YAxis, yrightAxis: YAxis): BarChartModel{
    let model: BarChartModel = new BarChartModel();
    model.mWidth = this.mWidth;
    model.mHeight = this.mHeight;
    model.init();
    model.setDrawBarShadow(false);
    model.setDrawValueAboveBar(true);
    model.getDescription().setEnabled(false);
    model.setMaxVisibleValueCount(60);
    model.setLeftYAxis(yleftAxis);
    model.setRightYAxis(yrightAxis);
    model.setXAxis(xAxis)
    model.prepareMatrixValuePx();
    return model;
  }

  build() {
    Column() {

      Column() {
        List({ space: 20, initialIndex: 0 }) {
          ForEach(this.list.map((data, index) => {
            let value: valueItem ={data: data, index: index};
            return value;
          }), (item:valueItem) => {
            ListItem() {
              Column() {
                BarChart({ model: item.data })
                MultipleLegend({ legend: this.legendLineList[item.index] });
              }
            }.editable(false)
          }, (item:valueItem) => item.index+"")
        }
        .listDirection(Axis.Vertical) // 排列方向
        .divider({ strokeWidth: 1, color: Color.Gray, startMargin: 0, endMargin: 0 }) // 每行之间的分界线
        .edgeEffect(EdgeEffect.None) // 滑动到边缘无效果
        .chainAnimation(false) // 联动特效关闭
      }
    }
  }

  private setData(cnt: number, model: BarChartModel, xAxis: XAxis, yLeftAxis: YAxis, yRightAxis: YAxis) {

    let entries: JArrayList<BarEntry> = new JArrayList<BarEntry>();

    for (let i = 0; i < 12; i++) {
      entries.add(new BarEntry(i, (Math.random() * 70) + 30));
    }

    let d: BarDataSet = new BarDataSet(entries, "New DataSet " + cnt);
    d.setColorsByVariable(ColorTemplate.VORDIPLOM_COLORS);
    d.setDrawIcons(false);

    let sets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
    sets.add(d);

    let cd: BarData = new BarData(sets);
    cd.setValueTextSize(10);
    // cd.setBarWidth(0);

    model.setData(cd);

    this.list.push(model);
  }
}
