/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Legend, LegendForm} from '@ohos/mpchart';
import { LineChart,LineChartModel } from '@ohos/mpchart';
import {XAxis, XAxisPosition} from '@ohos/mpchart';
import {YAxis,AxisDependency, YAxisLabelPosition} from '@ohos/mpchart';
import { LineData } from '@ohos/mpchart';
import {LineDataSet,ColorStop,Mode} from '@ohos/mpchart';
import { EntryOhos } from '@ohos/mpchart';
import {JArrayList} from '@ohos/mpchart';
import { ILineDataSet } from '@ohos/mpchart';
import {LimitLine,LimitLabelPosition} from '@ohos/mpchart';

@Entry
@Component
struct Index {
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  lineData: LineData | null = null;
  XLimtLine:LimitLine= new LimitLine(50, "Index 10");
  lineChartModel: LineChartModel = new LineChartModel();

  aboutToAppear() {

    this.lineData = this.initCurveData(45, 100);

    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(this.lineData.getXMin());
    this.topAxis.setAxisMaximum(this.lineData.getXMax());
    this.topAxis.enableGridDashedLine(10,10,0)

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(this.lineData.getXMin());
    this.bottomAxis.setAxisMaximum(this.lineData.getXMax());

    this.XLimtLine.setLineWidth(4);
    this.XLimtLine.enableDashedLine(10, 10,0);
    this.XLimtLine.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
    this.XLimtLine.setTextSize(10);
    this.XLimtLine.setTypeface(FontWeight.Normal);
    this.topAxis.addLimitLine(this.XLimtLine);
    this.topAxis.setDrawLimitLinesBehindData(true)

    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(5, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(this.lineData.getYMin());
    this.leftAxis.setAxisMaximum(this.lineData.getYMax());
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(this.lineData.getYMin()); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.lineData.getYMax());

    let ll1:LimitLine = new LimitLine(70, "Upper Limit");
    ll1.setLineWidth(4);
    ll1.enableDashedLine(10, 10, 0);
    ll1.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
    ll1.setTextSize(10);

    let ll2:LimitLine = new LimitLine(50, "Lower Limit");
    ll2.setLineWidth(4);
    ll2.enableDashedLine(10, 10, 0);
    ll2.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
    ll2.setTextSize(10);

    let ll3:LimitLine = new LimitLine(30, "第三条");
    ll3.setLineWidth(4);
    ll3.enableDashedLine(10, 10, 0);
    ll3.setLabelPosition(LimitLabelPosition.LEFT_TOP);
    ll3.setTextSize(10);

    let ll4:LimitLine = new LimitLine(10, "第四条");
    ll4.setLineWidth(4);
    ll4.enableDashedLine(10, 10, 0);
    ll4.setLabelPosition(LimitLabelPosition.LEFT_BOTTOM);
    ll4.setTextSize(10);

    //  draw limit lines behind data instead of on top
    this.leftAxis.setDrawLimitLinesBehindData(true);
    this.leftAxis.addLimitLine(ll1);
    this.leftAxis.addLimitLine(ll2);
    this.leftAxis.addLimitLine(ll3);
    this.leftAxis.addLimitLine(ll4);


    this.lineChartModel.setTopAxis(this.topAxis);
    this.lineChartModel.setBottomAxis(this.bottomAxis);
    this.lineChartModel.setWidth(this.mWidth);
    this.lineChartModel.setHeight(this.mHeight);
    this.lineChartModel.setMinOffset(this.minOffset);
    if (this.leftAxis) {
      this.lineChartModel.setLeftAxis(this.leftAxis);
    }
    if (this.rightAxis) {
      this.lineChartModel.setRightAxis(this.rightAxis);
    }
    if (this.lineData) {
      this.lineChartModel.setLineData(this.lineData);
    }
    this.lineChartModel.init();
  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private initCurveData(count: number, range: number): LineData {

    let values = new JArrayList<EntryOhos>();
    values.add(new EntryOhos(-50, 0));
    values.add(new EntryOhos(40, 100));
    values.add(new EntryOhos(80, -40));
    values.add(new EntryOhos(120, 120));
    values.add(new EntryOhos(250, 0));

    let values2 = new JArrayList<EntryOhos>();
    values2.add(new EntryOhos(10, 0));
    values2.add(new EntryOhos(60, 100));
    values2.add(new EntryOhos(110, 40));
    values2.add(new EntryOhos(160, 100));
    values2.add(new EntryOhos(210, 0));

    let values3 = new JArrayList<EntryOhos>();
    values3.add(new EntryOhos(30, 0));
    values3.add(new EntryOhos(80, 100));
    values3.add(new EntryOhos(130, 40));
    values3.add(new EntryOhos(180, 100));
    values3.add(new EntryOhos(230, 0));

    let gradientFillColor = new Array<ColorStop>();
    gradientFillColor.push([0x0C0099CC, 0.2])
    gradientFillColor.push([0x7F0099CC, 0.4])
    gradientFillColor.push([0x0099CC, 1.0])

    let dataSet = new JArrayList<ILineDataSet>();

    let set1 = new LineDataSet(values, "DataSet 1");
    set1.setMode(Mode.CUBIC_BEZIER)
    set1.setDrawFilled(false);
    set1.setGradientFillColor(gradientFillColor)
    set1.setColorByColor(Color.Green);
    set1.setLineWidth(2)
    set1.setDrawCircles(true);
    set1.setCircleColor(Color.Blue);
    set1.setCircleRadius(8);
    set1.setCircleHoleRadius(4)
    set1.setCircleHoleColor(Color.Green)
    set1.setDrawCircleHole(true)
    dataSet.add(set1);

    let set2 = new LineDataSet(values2, "DataSet 2");
    set2.setDrawFilled(false);
    set2.setGradientFillColor(gradientFillColor)
    set2.setColorByColor(Color.Orange);
    set2.setLineWidth(2)
    set2.setDrawCircles(true);
    set2.setCircleColor(Color.Red);
    set2.setCircleRadius(8);
    set2.setDrawCircleHole(false);
    dataSet.add(set2);

    let set3 = new LineDataSet(values3, "DataSet 3");
    set3.setDrawFilled(false);
    set3.setGradientFillColor(gradientFillColor)
    set3.setColorByColor(Color.Red);
    set3.setLineWidth(2)
    set3.setDrawCircles(false);
    set3.setCircleColor(Color.Orange);
    set3.setCircleRadius(16);
    dataSet.add(set3);

    return new LineData(dataSet)
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      LineChart({lineChartModel: $lineChartModel})
    }
  }
}