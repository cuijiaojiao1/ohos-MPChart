/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ColorTemplate } from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart'
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import {BarChart,BarChartModel} from '@ohos/mpchart'
import { IBarDataSet } from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct Index_bar_chart_2 {

  @State model:BarChartModel = new BarChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders','Animate X','Animate Y','Animate XY'];
  //标题栏标题
  private title: string = 'BarChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  //标题栏菜单回调
  menuCallback(){
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if(index==undefined||index==-1){
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for(let i = 0;i < this.model.getBarData().getDataSets().length();i++){
          let barDataSet = this.model.getBarData().getDataSets().get(i) as BarDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1?0:1)
        }
        this.model.invalidate()
        break;
      case 'Animate X':
        this.model.animateX(2000)
        break;
      case 'Animate Y':
        this.model.animateY(2000)
        break;
      case 'Animate XY':
        this.model.animateXY(2000,2000)
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear(){
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(110);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(110);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(10);

    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
    this.model.animateY(2000)

  }

  build() {
    Column(){
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }) {
        BarChart({model:this.model})
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private setData(count: number, range: number) {

    let start: number = 0;

    let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();

    for (let i = start; i < start + count; i++) {
      let multi = range;
      let val: number = (Math.random() * multi);
      values.add(new BarEntry(i, val))
    }

    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "Data Set");

      set1.setColorsByVariable(ColorTemplate.VORDIPLOM_COLORS);
      set1.setDrawValues(false);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);

      this.model.setData(data);
      this.model.setFitBars(true);
    }
  }
}