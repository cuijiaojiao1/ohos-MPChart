/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImagePaint } from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart'
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import { IBarDataSet } from '@ohos/mpchart'
import { HorizontalBarChart,HorizontalBarChartModel } from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct Index_Horizontal_Stacted2_Chart {
  @State model:HorizontalBarChartModel = new HorizontalBarChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 500; //表的高度
  minOffset: number = 15; //X轴线偏移量0
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis();
  //标题栏菜单文本
  private menuItemArr: Array<string> = ['Toggle Bar Borders','Animate X','Animate Y','Animate XY'];
  //标题栏标题
  private title: string = 'BarChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  //标题栏菜单回调
  menuCallback(){
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if(index==undefined||index==-1){
      return
    }
    switch (this.menuItemArr[index]) {
      case 'Toggle Bar Borders':
        for(let i = 0;i < this.model.getBarData().getDataSets().length();i++){
          let barDataSet = this.model.getBarData().getDataSets().get(i) as BarDataSet;
          barDataSet.setBarBorderWidth(barDataSet.getBarBorderWidth() == 1?0:1)
        }
        this.model.invalidate()
        break;
      case 'Animate X':
        this.model.animateX(2000)
        break;
      case 'Animate Y':
        this.model.animateY(2000)
        break;
      case 'Animate XY':
        this.model.animateXY(2000,2000)
        break;

    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear(){
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(11, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(110);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setLabelCount(11, false);
    this.rightAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(0);
    this.rightAxis.setAxisMaximum(110);
    this.rightAxis.setDrawLabels(true);


    this.topAxis.setLabelCount(6, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(-25);
    this.topAxis.setAxisMaximum(25);

    this.bottomAxis.setLabelCount(6, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(-25);
    this.bottomAxis.setAxisMaximum(25);

    this.setData()

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setTopXAxis(this.topAxis);
    this.model.setBottomXAxis(this.bottomAxis)
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.getDescription().setEnabled(false);
    this.model.setMaxVisibleValueCount(60);
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();

  }

  build() {
    Column(){
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }){
        HorizontalBarChart({model:this.model})
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private setData() {

    let start: number = 1;

    let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();
    values.add(new BarEntry(5, [ -10, 10 ]));
    values.add(new BarEntry(15,[ -12, 13 ]));
    values.add(new BarEntry(25, [ -15, 15 ]));
    values.add(new BarEntry(35, [ -17, 17 ]));
    values.add(new BarEntry(45, [ -19, 20 ]));
    //values.add(new BarEntry(45, [-19, 20 ]));
    values.add(new BarEntry(55, [ -19, 19 ]));
    values.add(new BarEntry(65, [ -16, 16 ]));
    values.add(new BarEntry(75, [-13, 14 ]));
    values.add(new BarEntry(85, [ -10, 11 ]));
    values.add(new BarEntry(95, [-5, 6 ]));
    values.add(new BarEntry(105, [-1, 2 ]));
    values.dataSource.reverse()
    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "Age Distribution");

      set1.setDrawIcons(false);
      set1.setColorsByArr([0x434348, 0x7cb5ec]);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);
      data.setValueTextSize(10);
      // data.setBarWidth(0);


      this.model.setData(data);
      this.model.setFitBars(true);
    }
  }
}